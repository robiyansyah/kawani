// store.js

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null,
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
  },
  actions: {
    loginUser({ commit }, userData) {
      // Kirim permintaan login ke server
      // Setelah berhasil, terima data pengguna dari server
      // Misalnya, userData adalah respons dari server
      const user = userData;
      commit('setUser', user); // Panggil mutation untuk menyimpan data pengguna
    },
  },
  getters: {
    user(state) {
      return state.user;
    },
  },
});