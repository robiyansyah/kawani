<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [Api\AuthController::class, 'login']);
Route::post('register', [Api\RegisterController::class, 'register']);
Route::post('forgot', [Api\ForgotController::class, 'forgot']);
Route::post('reset', [Api\ForgotController::class, 'reset']);
Route::get('email/resend/{user}', [Api\VerifyController::class, 'resend'])->name('verification.resend');
Route::get('email/verify/{id}', [Api\VerifyController::class, 'verify'])->name('verification.verify');; // Make sure to keep this as your route name
    
Route::group(['middleware' => ['auth:api']], function () {
    Route::get('user', [Api\AuthController::class, 'user']);
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::get('/paket-spse','App\Http\Controllers\Category@paketSPSE');
    Route::get('/paket-all','App\Http\Controllers\Category@daftarPaket');
    Route::get('/paket-all-review','App\Http\Controllers\Category@daftarPaketReview');
    Route::get('/dashboard-perpaket','App\Http\Controllers\Category@dashboardProgresPerPaket');
    Route::get('/dashboard-perdinas','App\Http\Controllers\Category@dashboardProgresPerDinas');
    Route::post('/edit-progres', 'Category@saveProgres');
    Route::get('/satker', 'App\Http\Controllers\Category@satker');
    Route::get('/dashboard-chart', 'App\Http\Controllers\Category@dashboardChartSummary');
    Route::get('/dashboard-chart-nontender', 'App\Http\Controllers\Category@dashboardChartSummryNontender');
    Route::post('/upload-tkdn-persiapan-pengadaan', 'App\Http\Controllers\Category@buktitkdnPersiapanPengadaan');
    Route::post('/upload-tkdn-kontrak', 'App\Http\Controllers\Category@buktitkdnKontrak');
    Route::post('/upload-tkdn-serah-terima', 'App\Http\Controllers\Category@buktitkdnSerahTerima');
    Route::post('/upload-kktkdn', 'App\Http\Controllers\Category@buktiKKTKDN');
    Route::get('/ranking-realisasi', 'App\Http\Controllers\Category@rankingRealisasi');
    Route::get('/rekap-all-dinas', 'App\Http\Controllers\Category@rekapAllDinas');
    Route::get('/rekap-paket-pdn', 'App\Http\Controllers\Category@perbandinganPaketPDN');
    Route::get('/top-sopd', 'App\Http\Controllers\Category@top5pdn');
    Route::get('/paket-perkategori', 'App\Http\Controllers\Category@paketPDNbyMetodePengadaan');
    Route::get('/progres-perkategori', 'App\Http\Controllers\Category@progresPerkategori');
    Route::get('/total-paket-pengadaan', 'App\Http\Controllers\Category@totalPaketPengadaan');
    Route::get('/realisasi-pengadaan', 'App\Http\Controllers\Category@realisasiPengadaan');
    Route::get('/realisasi-pdn', 'App\Http\Controllers\Category@realisasiPDN');
    Route::get('/realisasi-tkdn', 'App\Http\Controllers\Category@realisasiTKDN');
    Route::get('/summary-all', 'App\Http\Controllers\Category@summaryAll');
});
