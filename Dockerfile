FROM php:7.4-fpm

# Set working directory
WORKDIR /var/www

# buka kode dibawah ini jika install deb package gagal
#RUN echo "deb http://ftp.au.debian.org/debian/ buster main contrib non-free" > /etc/apt/sources.list
#RUN echo "deb-src http://ftp.au.debian.org/debian/ buster main contrib non-free" >> /etc/apt/sources.list

#RUN echo "deb http://mirror.linux.org.au/debian buster main contrib non-free" > /etc/apt/sources.list
#RUN echo "deb-src http://mirror.linux.org.au/debian buster main contrib non-free" >> /etc/apt/sources.list

RUN apt-get update
RUN apt-get install -y netselect-apt apt-utils gnupg

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update

# development packages
RUN apt-get install -y \
    build-essential \
    git \
    zip \
    curl \
    wget \
    nano \
    locales \
    libicu-dev \
    libbz2-dev \
    libzip-dev \
    zlib1g-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libfreetype6-dev \
    libpq-dev \
    libz-dev \
    libssl-dev \
    libsqlite3-dev \
    libsqlite3-0 \
    libxml2-dev \
    libreadline-dev \
    nodejs netcat yarn \
    g++

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen \
    && apt-get update

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath bz2 \
    pdo \
    intl \
    soap \
    pcntl \
    bcmath \
    simplexml \
    pdo_sqlite \
    pdo_mysql \
    pdo_pgsql \
    zip

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
# RUN useradd -G www-data,root -u $uid -d /home/$user $user
# RUN mkdir -p /home/$user/.composer && \
#     chown -R $user:$user /home/$user

#RUN docker-php-ext-configure gd --with-gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-png-dir=/usr/include/
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install gd

#RUN pecl install xdebug redis uopz && docker-php-ext-enable xdebug redis

# composer
# RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents
COPY . .

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

# Change current user to www
USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
