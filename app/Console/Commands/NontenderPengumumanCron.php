<?php

namespace App\Console\Commands;

use App\Models\NonTenderPengumuman;
use App\Models\SpsePaket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class NontenderPengumumanCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nontenderpengumuman:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Non Tender Pengumuman Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Checking url......');
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/88641e17-5098-40ec-a72d-6e82b3e61272/json/2819/SPSE-NonTenderPengumuman/tipe/4:4/parameter/2024:14');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        $this->info('Truncate Data ......');
        NonTenderPengumuman::where('tahun_anggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data ......');
        $this->output->progressStart(count($program));
        foreach($program as $res => $value) {
            $this->output->progressAdvance();
            NonTenderPengumuman::insert([
                'tahun_anggaran' => $value['tahun_anggaran'],
                'kd_klpd' => $value['kd_klpd'],
                'nama_klpd' => $value['nama_klpd'],
                'jenis_klpd' => $value['jenis_klpd'],
                'kd_satker' => $value['kd_satker'],
                'kd_satker_str' => $value['kd_satker_str'],
                'nama_satker' => $value['nama_satker'],
                'kd_lpse' => $value['kd_lpse'],
                'nama_lpse' => $value['nama_lpse'],
                'kd_nontender' => $value['kd_nontender'],
                'kd_pkt_dce' => $value['kd_pkt_dce'],
                'kd_rup' => $value['kd_rup'],
                'nama_paket' => $value['nama_paket'],
                'pagu' => $value['pagu'],
                'hps' => $value['hps'],
                'sumber_dana' => $value['sumber_dana'],
                'mak' => $value['mak'],
                'kualifikasi_paket' => $value['kualifikasi_paket'],
                'jenis_pengadaan' => $value['jenis_pengadaan'],
                'mtd_pemilihan' => $value['mtd_pemilihan'],
                'kontrak_pembayaran' => $value['kontrak_pembayaran'],
                'status_nontender' => $value['status_nontender'],
                'versi_nontender' => $value['versi_nontender'],
                'ket_diulang' => $value['ket_diulang'],
                'ket_ditutup' => $value['ket_ditutup'],
                'tgl_buat_paket' => $value['tgl_buat_paket'],
                'tgl_kolektif_kolegial' => $value['tgl_kolektif_kolegial'],
                'tgl_pengumuman_nontender' => $value['tgl_pengumuman_nontender'],
                'dibuat_oleh' => $value['nip_nama_pp'],
                'nip_pembuat_paket' => $value['nip_nama_pp'],
                'nama_pembuat_paket' => $value['nip_nama_ppk'],
                'lokasi_pekerjaan' => $value['lokasi_pekerjaan'],
                'url_lpse' => $value['url_lpse']
            ]);

        }
        $this->output->progressFinish();
        Log::info("Non Tender Pengumuman Cron execution!");
        $this->info('Non Tender Pengumuman:Cron Command is working fine!');
    }
}
