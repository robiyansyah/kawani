<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SirupObjekAkun;

class ObjekAkunCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'objekakun:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Objek Akun updated successfull!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://isb.lkpp.go.id/isb/api/91e0b971-96df-43c7-b7a1-3a5555228013/json/201100482/ObjekAkunMasterRUP/tipe/4:12/parameter/2023:D95');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        foreach($program as $res => $value) {
            SirupObjekAkun::updateOrCreate([
                'id_program' => $value['id_program'],
                'id_kegiatan' => $value['id_kegiatan'],
                'id_objek_akun' => $value['id'],
                'kode_objekakund' => $value['kode_objekakund'],
                'uraian_objekakun' => $value['uraian_objekakun'],
                'pagu' => $value['pagu'],
                'id_client' => $value['id_client'],
                'is_deleted' => $value['is_deleted']
            ]);
        }
        Log::info("ObjekAkun Cron execution!");
        $this->info('ObjekAkun:Cron Command is working fine!');
    }
}
