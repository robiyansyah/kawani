<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SirupKegiatan;

class KegiatanCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kegiatan:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kegiatan Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://isb.lkpp.go.id/isb/api/211b2423-bae6-4d05-8067-f990c554e85c/json/141625383/KegiatanMasterRUP/tipe/4:12/parameter/2024:D95');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        foreach($program as $res => $value) {
            SirupKegiatan::updateOrCreate([
                'id_program' => $value['id_program'],
                'id_kegiatan' => $value['id'],
                'kode_kegiatans' => $value['kode_kegiatans'],
                'nama' => $value['nama'],
                'pagu' => $value['pagu'],
                'id_client' => $value['id_client'],
                'is_deleted' => $value['is_deleted']
            ])->where('id_kegiatan', $value['id']);
        }
        Log::info("Kegiatan Cron execution!");
        $this->info('Kegiatan:Cron Command is working fine!');
    }
}
