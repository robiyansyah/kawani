<?php

namespace App\Console\Commands;

use App\Models\NonTenderEkontrak;
use App\Models\SpsePaket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class NontenderEkontrakCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nontenderekontrak:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Non Tender Ekontrak Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Checking url......');
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/3c94eefc-caa5-46c8-bf0a-9a9a1f2b2d69/json/6531/SPSE-NonTenderEkontrak-Kontrak/tipe/4:4/parameter/2024:14');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        NonTenderEkontrak::where('tahun_anggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data ......');
        $this->output->progressStart(count($program));
        foreach($program as $res => $value) {
            $this->output->progressAdvance();
            NonTenderEkontrak::insert([
                'tahun_anggaran' => $value['tahun_anggaran'],
                'kd_klpd' => $value['kd_klpd'],
                'nama_klpd' => $value['nama_klpd'],
                'jenis_klpd' => $value['jenis_klpd'],
                'kd_satker' => $value['kd_satker'],
                'kd_satker_str' => $value['kd_satker_str'],
                'nama_satker' => $value['nama_satker'],
                'alamat_satker' => $value['alamat_satker'],
                'kd_lpse' => $value['kd_lpse'],
                'kd_nontender' => $value['kd_nontender'],
                'nama_paket' => $value['nama_paket'],
                'mtd_pengadaan' => $value['mtd_pengadaan'],
                'lingkup_pekerjaan' => $value['lingkup_pekerjaan'],
                'no_sppbj' => $value['no_sppbj'],
                'no_kontrak' => $value['no_kontrak'],
                'tgl_kontrak' => $value['tgl_kontrak'],
                'tgl_kontrak_awal' => $value['tgl_kontrak_awal'],
                'tgl_kontrak_akhir' => $value['tgl_kontrak_akhir'],
                'kota_kontrak' => $value['kota_kontrak'],
                'nip_ppk' => $value['nip_ppk'],
                'nama_ppk' => $value['nama_ppk'],
                'jabatan_ppk' => $value['jabatan_ppk'],
                'no_sk_ppk' => $value['no_sk_ppk'],
                'nama_penyedia' => $value['nama_penyedia'],
                'npwp_penyedia' => $value['npwp_penyedia'],
                'bentuk_usaha_penyedia' => $value['bentuk_usaha_penyedia'],
                'tipe_penyedia' => $value['tipe_penyedia'],
                'anggota_kso' => $value['anggota_kso'],
                'wakil_sah_penyedia' => $value['wakil_sah_penyedia'],
                'jabatan_wakil_penyedia' => $value['jabatan_wakil_penyedia'],
                'nama_rek_bank' => $value['nama_rek_bank'],
                'no_rek_bank' => $value['no_rek_bank'],
                'nama_pemilik_rek_bank' => $value['nama_pemilik_rek_bank'],
                'nilai_kontrak' => $value['nilai_kontrak'],
                'alasan_ubah_nilai_kontrak' => $value['alasan_ubah_nilai_kontrak'],
                'alasan_nilai_kontrak_10_persen' => $value['alasan_nilai_kontrak_10_persen'],
                'nilai_pdn_kontrak' => $value['nilai_pdn_kontrak'],
                'nilai_umk_kontrak' => $value['nilai_umk_kontrak'],
                'jenis_kontrak' => $value['jenis_kontrak'],
                'informasi_lainnya' => $value['informasi_lainnya'],
                'status_kontrak' => $value['status_kontrak'],
                'tgl_penetapan_status_kontrak' => $value['tgl_penetapan_status_kontrak'],
                'alasan_penetapan_status_kontrak' => $value['alasan_penetapan_status_kontrak'],
                'apakah_addendum' => $value['apakah_addendum'],
                'versi_addendum' => $value['versi_addendum'],
                'alasan_addendum' => $value['alasan_addendum']
            ]);

        }
        $this->output->progressFinish();
        Log::info("Non Tender Ekontrak Cron execution!");
        $this->info('Non Tender Ekontrak:Cron Command is working fine!');
    }
}
