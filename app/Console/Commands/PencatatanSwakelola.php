<?php

namespace App\Console\Commands;

use App\Models\PencatatanSwakelola as ModelsPencatatanSwakelola;
use App\Models\Tender;
use App\Models\SpsePaket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PencatatanSwakelola extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pencatatan-swakelola:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pencatatan Swakelola Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/f06d47f7-3b86-4f52-ba3f-4e0a486bca76/json/11604/SPSE-PencatatanSwakelola/tipe/4:4/parameter/2024:14');
        $response = $request->getBody()->getContents();
        $dataTenderSelesai = json_decode($response, true);

        $this->info('Truncate Data Pencatatan Swakelola');
        ModelsPencatatanSwakelola::where('tahun_anggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data Pencatatan Swakelola');
        $this->output->progressStart(count($dataTenderSelesai));
        foreach($dataTenderSelesai as $res => $value) {
            $this->output->progressAdvance();
            ModelsPencatatanSwakelola::insert([
                'tahun_anggaran' => $value['tahun_anggaran'],
                'kd_klpd' => $value['kd_klpd'],
                'nama_klpd' => $value['nama_klpd'],
                'jenis_klpd' => $value['jenis_klpd'],
                'kd_satker' => $value['kd_satker'],
                'kd_satker_str' => $value['kd_satker_str'],
                'nama_satker' => $value['nama_satker'],
                'kd_lpse' => $value['kd_lpse'],
                'kd_swakelola_pct' => $value['kd_swakelola_pct'],
                'kd_pkt_dce' => $value['kd_pkt_dce'],
                'kd_rup' => $value['kd_rup'],
                'nama_paket' => $value['nama_paket'],
                'pagu' => $value['pagu'],
                'total_realisasi' => $value['total_realisasi'],
                'nilai_pdn_pct' => $value['nilai_pdn_pct'],
                'nilai_umk_pct' => $value['nilai_umk_pct'],
                'sumber_dana' => $value['sumber_dana'],
                'uraian_pekerjaan' => $value['uraian_pekerjaan'],
                'informasi_lainnya' => $value['informasi_lainnya'],
                'tipe_swakelola' => $value['tipe_swakelola'],
                'tipe_swakelola_nama' => $value['tipe_swakelola_nama'],
                'status_swakelola_pct' => $value['status_swakelola_pct'],
                'status_swakelola_pct_ket' => $value['status_swakelola_pct_ket'],
                'alasan_pembatalan' => $value['alasan_pembatalan'],
                'nip_ppk' => $value['nip_ppk'],
                'nama_ppk' => $value['nama_ppk'],
                'tgl_buat_paket' => $value['tgl_buat_paket'],
                'tgl_mulai_paket' => $value['tgl_mulai_paket'],
                'tgl_selesai_paket' => $value['tgl_selesai_paket']

            ]);
        }

        // $req = $client->get('https://dce.lkpp.go.id/isb-2/api/f72653f5-9cd4-4c47-aaac-9a96c923deb2/json/10297/SPSE-TenderSelesaiNilai/tipe/4:4/parameter/2023:14');
        // $resp = $req->getBody()->getContents();
        // $nilaiTender = json_decode($resp, true);
        // foreach($dataTenderSelesai as $res => $value) {
        //     SpsePaket::updateOrCreate([
        //         'ang_tahun' => $value['tahun_anggaran'],
        //         'kd_klpd' => $value['kd_klpd'],  //add db
        //         'nama_klpd' => $value['nama_klpd'], //add db
        //         'jenis_klpd' => $value['jenis_klpd'], //add db
        //         'idsatker' => $value['kd_satker'],
        //         'kd_satker_str' => $value['kd_satker_str'], //add db
        //         'stk_nama' => $value['nama_satker'],
        //         'kd_lpse' => $value['kd_lpse'], //add db
        //         'nama_lpse' => $value['nama_lpse'], //add db
        //         'idlelang' => $value['kd_tender'],
        //         'kd_pkt_dce' => $value['kd_pkt_dce'], //add db
        //         'rup_id' => $value['kd_rup'],
        //         'pkt_nama' => $value['nama_paket'],
        //         'pkt_pagu' => $value['pagu'],
        //         'pkt_hps' => $value['hps'],
        //         'sumber_dana' => $value['sumber_dana'], //add db
        //         'mak' => $value['mak'], //add db
        //         'kualifikasi_paket' => $value['kualifikasi_paket'], //add db
        //         'mtd_pemilihan' => $value['mtd_pemilihan'], //add db
        //         'mtd_evaluasi' => $value['mtd_evaluasi'], //add db
        //         'mtd_kualifikasi' => $value['mtd_kualifikasi'], //add db
        //         'kontrak_pembayaran' => $value['kontrak_pembayaran'], //add db
        //         'tahapan' => $value['status_tender'],
        //         'lls_tgl_setuju' => $value['tgl_pengumuman_tender'],
        //         'tgl_penetapan_pemenang' => $value['tgl_penetapan_pemenang']//add db

        //         // 'penawaran' => $value['nilai_penawaran'],
        //         // 'pemenang' => $value['nama_penyedia'],
        //         // 'kontrak_nilai' => $value['nilai_kontrak'],
        //         // 'realisasi_pdn' => $value['nilai_pdn_kontrak'],
        //         // 'realisasi_umk' => $value['nilai_umk_kontrak']

        //     ])->where('rup_id', $value['kd_rup']);
        // }

        // foreach($nilaiTender as $res => $value) {
        //     SpsePaket::where('rup_id', $value['kd_rup_paket'])
        //     ->update([
        //         'penawaran' => $value['nilai_penawaran'],
        //         'pemenang' => $value['nama_penyedia'],
        //         'nilai_terkoreksi' => $value['nilai_terkoreksi'], //add db
        //         'nilai_negosiasi' => $value['nilai_negosiasi'], //add db
        //         'kontrak_nilai' => $value['nilai_kontrak'],
        //         'realisasi_pdn' => $value['nilai_pdn_kontrak'],
        //         'realisasi_umk' => $value['nilai_umk_kontrak']
        //     ]);
        // }

        $this->output->progressFinish();
        Log::info(" Tender Selesai Cron execution!");
        $this->info(' Tender Selesai:Cron Command is working fine!');
    }
}
