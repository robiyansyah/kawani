<?php

namespace App\Console\Commands;

use App\Models\PencatatanNontender;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SpsePaket;

class PencatatanNonTenderSelesaiCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pencatatan-nontender:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pencatatan Non Tender Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Checking url......');
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/dbe0cbe1-3c10-4309-99b4-c6f985c359b5/json/11606/SPSE-PencatatanNonTender/tipe/4:4/parameter/2024:14');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        PencatatanNontender::where('tahun_anggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data ......');
        $this->output->progressStart(count($program));
        foreach($program as $res => $value) {
            $this->output->progressAdvance();
            PencatatanNontender::insert([
                'tahun_anggaran' => $value['tahun_anggaran'],
                'kd_klpd' => $value['kd_klpd'],
                'nama_klpd' => $value['nama_klpd'],
                'jenis_klpd' => $value['jenis_klpd'],
                'kd_satker' => $value['kd_satker'],
                'kd_satker_str' => $value['kd_satker_str'],
                'nama_satker' => $value['nama_satker'],
                'kd_lpse' => $value['kd_lpse'],
                'kd_nontender_pct' => $value['kd_nontender_pct'],
                'kd_pkt_dce' => $value['kd_pkt_dce'],
                'kd_rup' => $value['kd_rup'],
                'nama_paket' => $value['nama_paket'],
                'pagu' => $value['pagu'],
                'total_realisasi' => $value['total_realisasi'],
                'nilai_pdn_pct' => $value['nilai_pdn_pct'],
                'nilai_umk_pct' => $value['nilai_umk_pct'],
                'sumber_dana' => $value['sumber_dana']	,
                'uraian_pekerjaan' => $value['uraian_pekerjaan'],
                'informasi_lainnya' => $value['informasi_lainnya'],
                'kategori_pengadaan' => $value['kategori_pengadaan'],
                'mtd_pemilihan' => $value['mtd_pemilihan'],
                'bukti_pembayaran' => $value['bukti_pembayaran'],
                'status_nontender_pct' => $value['status_nontender_pct'],
                'status_nontender_pct_ket' => $value['status_nontender_pct_ket'],
                'alasan_pembatalan' => $value['alasan_pembatalan'],
                'nip_ppk' => $value['nip_ppk'],
                'nama_ppk' => $value['nama_ppk'],
                'tgl_buat_paket' => $value['tgl_buat_paket'],
                'tgl_mulai_paket' => $value['tgl_mulai_paket'],
                'tgl_selesai_paket' => $value['tgl_selesai_paket']
            ]);
        }
        $this->output->progressFinish();
        Log::info("Pencatatan Non Tender Cron execution!");
        $this->info('Pencatatan Non Tender:Cron Command is working fine!');

    }
}
