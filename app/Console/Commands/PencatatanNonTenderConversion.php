<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SpsePaket;

class PencatatanNonTenderConversion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pencatatan-nontender-all-conversion:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pencatatan Non Tender ALL conversion Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Update Query......');
        $data = DB::select(DB::raw("
            select sp.idrup, sp.namapaket, sp.namasatker, sp.tahunanggaran, sp.jumlahpagu, sp.ppk, sp.statustkdn, sp.statususahakecil, sp.statusumumkan, sp.metodepengadaan,
                pn.kd_nontender_pct, pn.pagu , pn.total_realisasi, pn.nilai_pdn_pct , pn.nilai_umk_pct , pn.bukti_pembayaran , pn.status_nontender_pct , pn.status_nontender_pct_ket , pn.nama_ppk ,
                pn.tgl_buat_paket , pn.tgl_mulai_paket , pn.tgl_selesai_paket
            from sirup_penyedia sp
                left join pencatatan_nontender pn on pn.kd_rup = sp.idrup
            where pn.kd_klpd = 'D95' and pn.tahun_anggaran = 2023
            "
            )
        );
        $this->info('Truncate lastest data......');
        DB::table('convert_pencatatan_nontender')->truncate();
        $this->info('Truncate Success!');
        $this->info('Update Data......');

            $tenderAllConversion = json_encode($data);
            $arr = json_decode($tenderAllConversion, true);
            $this->output->progressStart(count($arr));
            foreach($arr as $resp => $value) {
                $this->output->progressAdvance();
                    DB::table('convert_pencatatan_nontender')->insert([
                        'idrup' => $value['idrup'],
                        'namapaket' => $value['namapaket'],
                        'namasatker' => $value['namasatker'],
                        'tahunanggaran' => $value['tahunanggaran'],
                        'jumlahpagu' => $value['jumlahpagu'],
                        'ppk' => $value['ppk'],
                        'statustkdn' => $value['statustkdn'],
                        'statususahakecil' => $value['statususahakecil'],
                        'statusumumkan' => $value['statusumumkan'],
                        'metodepengadaan' => $value['metodepengadaan'],
                        'kd_nontender_pct' => $value['kd_nontender_pct'],
                        'pagu' => $value['pagu'],
                        'total_realisasi' => $value['total_realisasi'],
                        'nilai_pdn_pct' => $value['nilai_pdn_pct'],
                        'nilai_umk_pct' => $value['nilai_umk_pct'],
                        'bukti_pembayaran' => $value['bukti_pembayaran'],
                        'status_nontender_pct' => $value['status_nontender_pct'],
                        'status_nontender_pct_ket' => $value['status_nontender_pct_ket'],
                        'nama_ppk' => $value['nama_ppk'],
                        'tgl_buat_paket' => $value['tgl_buat_paket'],
                        'tgl_mulai_paket' => $value['tgl_mulai_paket'],
                        'tgl_selesai_paket' => $value['tgl_selesai_paket']
                    ]);
                }

        $this->output->progressFinish();

        // ================ Tender selesai insert ke table spse paket ======

        $QueryTenderSelesai = DB::select(DB::raw("
            select sp.idrup, sp.namapaket, sp.namasatker, sp.tahunanggaran, sp.jumlahpagu, sp.ppk, sp.statustkdn, sp.statususahakecil, sp.statusumumkan, sp.metodepengadaan,
                pn.kd_nontender_pct, pn.pagu , pn.total_realisasi, pn.nilai_pdn_pct , pn.nilai_umk_pct , pn.bukti_pembayaran , pn.status_nontender_pct , pn.status_nontender_pct_ket , pn.nama_ppk ,
                pn.tgl_buat_paket , pn.tgl_mulai_paket , pn.tgl_selesai_paket
            from sirup_penyedia sp
                left join pencatatan_nontender pn on pn.kd_rup = sp.idrup
            where pn.kd_klpd = 'D95' and pn.tahun_anggaran = 2023 and pn.status_nontender_pct = 'Aktif' and pn.status_nontender_pct_ket != 'Paket Dibatalkan'
            "
            )
        );
        $this->info('Delete old data tender selesai......');
        DB::table('spse_paket')
        ->where('kgr_id', 'Pencatatan Non Tender')
        ->delete();
        $this->info('Delete Success!');
        $this->info('Insert new Data......');

            $tenderSelesai = json_encode($QueryTenderSelesai);
            $array = json_decode($tenderSelesai, true);
            $this->output->progressStart(count($array));
            foreach($array as $resp => $value) {
                $this->output->progressAdvance();
                    DB::table('spse_paket')->insert([
                        'ang_tahun' => $value['tahunanggaran'],
                        'rup_id' => $value['idrup'],
                        'pkt_nama' => $value['namapaket'],
                        'stk_nama' => $value['namasatker'],
                        'pkt_pagu' => $value['pagu'],
                        'nama_ppk' => $value['ppk'],
                        'kgr_id' => 'Pencatatan Non Tender',
                        'statustkdn' => $value['statustkdn'],
                        'statususahakecil' => $value['statususahakecil'],
                        'statusumumkan' => $value['statusumumkan'],
                        'mtd_pemilihan' => $value['metodepengadaan'],
                        'idlelang' => $value['kd_nontender_pct'],
                        'tahapan' => $value['status_nontender_pct_ket'],
                        // 'lls_versi_lelang' => $value['versi_tender'],
                        'pkt_hps' => 0,
                        'lls_dibuat_tanggal' => $value['tgl_buat_paket'],
                        'lls_tgl_setuju' => $value['tgl_selesai_paket'],
                        'kontrak_no' => '-',
                        'tgl_kontrak' => $value['tgl_mulai_paket'],
                        'pemenang' => '-',
                        'kontrak_nilai' => $value['total_realisasi'],
                        'realisasi_pdn' => $value['nilai_pdn_pct'],
                        'realisasi_umk' => $value['nilai_umk_pct']
                    ]);
                }

        $this->output->progressFinish();
        Log::info("Tender All Cron execution!");
        $this->info('Tender All:Cron Command is working fine!');
    }
}
