<?php

namespace App\Console\Commands;

use App\Models\NonTender;
use App\Models\SpsePaket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class NontenderSelesai extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nontenderselesai:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Non Tender Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Cek url......');
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/fd011be0-7a8c-472b-b616-f2215e5f7805/json/2816/SPSE-NonTenderSelesai/tipe/4:4/parameter/2024:14');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        $this->info('Truncate Data ......');
        NonTender::where('tahun_anggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data ......');
        $this->output->progressStart(count($program));
        foreach($program as $res => $value) {
            $this->output->progressAdvance();
            NonTender::insert([
                'tahun_anggaran' => $value['tahun_anggaran'],
                'kd_klpd' => $value['kd_klpd'],
                'nama_klpd' => $value['nama_klpd'],
                'jenis_klpd' => $value['jenis_klpd'],
                'kd_satker' => $value['kd_satker'],
                'kd_satker_str' => $value['kd_satker_str'], //add db
                'nama_satker' => $value['nama_satker'],
                'kd_lpse' => $value['kd_lpse'],
                'nama_lpse' => $value['nama_lpse'],
                'kd_nontender' => $value['kd_nontender'],
                'kd_pkt_dce' => $value['kd_pkt_dce'], //add db
                'kd_rup' => $value['kd_rup'],
                'nama_paket' => $value['nama_paket'],
                'pagu' => $value['pagu'],
                'hps' => $value['hps'],
                'nilai_penawaran' => $value['nilai_penawaran'],
                'nilai_terkoreksi' => $value['nilai_terkoreksi'],
                'nilai_negosiasi' => $value['nilai_negosiasi'],
                'nilai_kontrak' => $value['nilai_kontrak'],
                'nilai_pdn_kontrak' => $value['nilai_pdn_kontrak'],
                'nilai_umk_kontrak' => $value['nilai_umk_kontrak'],
                'sumber_dana' => $value['sumber_dana'],  //add db
                'mak' => $value['mak'],
                'kualifikasi_paket' => $value['kualifikasi_paket'],
                'jenis_pengadaan' => $value['jenis_pengadaan'],  //add db
                'mtd_pemilihan' => $value['mtd_pemilihan'],  //add db
                'kontrak_pembayaran' => $value['kontrak_pembayaran'],  //add db
                'status_nontender' => $value['status_nontender'],  //add db
                'tgl_pengumuman_nontender' => $value['tgl_pengumuman_nontender'],
                'tgl_selesai_nontender' => $value['tgl_selesai_nontender'],
                'dibuat_oleh' => $value['nip_nama_ppk'], //add db
                'nip_nama_ppk' => $value['nip_nama_ppk'], //add db
                'nip_nama_pp' => $value['nip_nama_pp'], //add db
                'nip_nama_pokja' => $value['nip_nama_pokja'], //add db
                'kd_penyedia' => $value['kd_penyedia'],
                'nama_penyedia' => $value['nama_penyedia'],
                'npwp_penyedia' => $value['npwp_penyedia']

                // 'anggaran' => $value['anggaran'],
                // 'kategori_pengadaan' => $value['kategori_pengadaan'],
                // 'metode_pengadaan' => $value['metode_pengadaan'],
                // 'tanggal_buat_paket' => $value['tanggal_buat_paket'],
            ]);

            // SpsePaket::updateOrCreate([
            //     'ang_tahun' => $value['tahun_anggaran'],
            //     'kd_klpd' => $value['kd_klpd'],  //add db
            //     'nama_klpd' => $value['nama_klpd'], //add db
            //     'jenis_klpd' => $value['jenis_klpd'], //add db
            //     'idsatker' => $value['kd_satker'],
            //     'kd_satker_str' => $value['kd_satker_str'], //add db
            //     'stk_nama' => $value['nama_satker'],
            //     'kd_lpse' => $value['kd_lpse'], //add db
            //     'nama_lpse' => $value['nama_lpse'], //add db
            //     'idlelang' => $value['kd_nontender'],
            //     'kd_pkt_dce' => $value['kd_pkt_dce'], //add db
            //     'rup_id' => $value['kd_rup'],
            //     'pkt_nama' => $value['nama_paket'],
            //     'pkt_pagu' => $value['pagu'],
            //     'pkt_hps' => $value['hps'],
            //     'sumber_dana' => $value['sumber_dana'], //add db
            //     'mak' => $value['mak'], //add db
            //     'kualifikasi_paket' => $value['kualifikasi_paket'], //add db
            //     'mtd_pemilihan' => $value['mtd_pemilihan'], //add db
            //     // 'mtd_evaluasi' => $value['mtd_evaluasi'], //add db not use in non tender
            //     // 'mtd_kualifikasi' => $value['mtd_kualifikasi'], //add db
            //     'kontrak_pembayaran' => $value['kontrak_pembayaran'], //add db
            //     'tahapan' => $value['status_nontender'],
            //     'lls_tgl_setuju' => $value['tgl_pengumuman_nontender'],
            //     'tgl_penetapan_pemenang' => $value['tgl_selesai_nontender'],//add db
            //     'penawaran' => $value['nilai_penawaran'],
            //     'pemenang' => $value['nama_penyedia'],
            //     'nilai_terkoreksi' => $value['nilai_terkoreksi'], //add db
            //     'nilai_negosiasi' => $value['nilai_negosiasi'], //add db
            //     'kontrak_nilai' => $value['nilai_kontrak'],
            //     'realisasi_pdn' => $value['nilai_pdn_kontrak'],
            //     'realisasi_umk' => $value['nilai_umk_kontrak']
            // ]);
        }
        $this->output->progressFinish();
        Log::info("Non Tender Selesai Cron execution!");
        $this->info('Non Tender Selesai:Cron Command is working fine!');
    }
}
