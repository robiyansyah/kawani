<?php

namespace App\Console\Commands;

use App\Models\Ecatalogs;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SirupKegiatan;
use App\Models\SpsePaket;

class Ecatalog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ecatalog:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'E Catalog Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Checking url......');
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/0110a25b-fa0e-48db-abc1-1e02431f3da4/json/2812/Ecat-PaketEPurchasing/tipe/4:12/parameter/2024:D95');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        $this->info('Truncate Data 2024 ');
        Ecatalogs::where('tahun_anggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data ......');
        $this->output->progressStart(count($program));
        foreach($program as $res => $value) {
            $this->output->progressAdvance();
            Ecatalogs::insert([
                'tahun_anggaran' => $value['tahun_anggaran'],
                'kd_klpd' => $value['kd_klpd'],
                'satker_id' => $value['satker_id'],
                'nama_satker' => $value['nama_satker'],
                'alamat_satker' => $value['alamat_satker'],
                'npwp_satker' => $value['npwp_satker'],
                'kd_paket' => $value['kd_paket'],
                'no_paket' => $value['no_paket'],
                'nama_paket' => $value['nama_paket'],
                'kd_rup' => $value['kd_rup'],
                'nama_sumber_dana' => $value['nama_sumber_dana'],
                'kode_anggaran' => $value['kode_anggaran'],
                'kd_komoditas' => $value['kd_komoditas'],
                'kd_produk' => $value['kd_produk'],
                'kd_penyedia' => $value['kd_penyedia'],
                'kd_penyedia_distributor' => $value['kd_penyedia_distributor'],
                'jml_jenis_produk' => $value['jml_jenis_produk'],
                // 'total' => $value['total'],
                'kuantitas' => $value['kuantitas'],
                'harga_satuan' => $value['harga_satuan'],
                'ongkos_kirim' => $value['ongkos_kirim'],
                'total_harga' => $value['total_harga'],
                'kd_user_pokja' => $value['kd_user_pokja'],
                'no_telp_user_pokja' => $value['no_telp_user_pokja'],
                'email_user_pokja' => $value['email_user_pokja'],
                'kd_user_ppk' => $value['kd_user_ppk'],
                'ppk_nip' => $value['ppk_nip'],
                'jabatan_ppk' => $value['jabatan_ppk'],
                'tanggal_buat_paket' => $value['tanggal_buat_paket'],
                'tanggal_edit_paket' => $value['tanggal_edit_paket'],
                'deskripsi' => $value['deskripsi'],
                'status_paket' => $value['status_paket'],
                'paket_status_str' => $value['paket_status_str'],
                'catatan_produk' => $value['catatan_produk']
            ]);



            // $req = $client->get('https://isb.lkpp.go.id/isb-2/api/784c6df9-7502-43bf-9a43-92ac79f5a44c/json/10252/Ecat-PenyediaDetail/tipe/4/parameter/'.$value['kd_penyedia']);
            // $res = $req->getBody()->getContents();
            // $detail = json_decode($res, true);
            // foreach($detail as $resp => $val) {
            //     SpsePaket::updateOrCreate([
            //         'ang_tahun' => $value['tahun_anggaran'],
            //         'kd_klpd' => $value['kd_klpd'],  //add db
            //         // 'nama_klpd' => $value['nama_klpd'], //add db not use
            //         // 'jenis_klpd' => $value['jenis_klpd'], //add db not user
            //         'idsatker' => $value['satker_id'],
            //         // 'kd_satker_str' => $value['kd_satker_str'], //add db not use
            //         'stk_nama' => $value['nama_satker'],
            //         // 'kd_lpse' => $value['kd_lpse'], //add db not use
            //         // 'nama_lpse' => $value['nama_lpse'], //add db
            //         'idlelang' => $value['no_paket'],
            //         'kd_pkt_dce' => $value['kd_paket'], //add db
            //         'rup_id' => $value['kd_rup'],
            //         'pkt_nama' => $value['nama_paket'],
            //         // 'pkt_pagu' => $value['pagu'], not use
            //         // 'pkt_hps' => $value['hps'], not use
            //         'sumber_dana' => $value['nama_sumber_dana'],
            //         'mak' => $value['kode_anggaran'],
            //         // 'kualifikasi_paket' => $value['kualifikasi_paket'], //add db
            //         'mtd_pemilihan' => 'E-purchasing', //add db
            //         // 'mtd_evaluasi' => $value['mtd_evaluasi'], //add db not use in non tender
            //         // 'mtd_kualifikasi' => $value['mtd_kualifikasi'], //add db
            //         // 'kontrak_pembayaran' => $value['kontrak_pembayaran'], //add db
            //         'tahapan' => $value['paket_status_str'],
            //         'lls_tgl_setuju' => $value['tanggal_buat_paket'],
            //         'tgl_penetapan_pemenang' => $value['tanggal_edit_paket'],//add db
            //         // 'penawaran' => $value['nilai_penawaran'],
            //         // 'pemenang' => 'https://isb.lkpp.go.id/isb-2/api/784c6df9-7502-43bf-9a43-92ac79f5a44c/json/10252/Ecat-PenyediaDetail/tipe/4/parameter/'.$value['kd_penyedia'],
            //         'pemenang' => $val['nama_penyedia'],
            //         // 'nilai_terkoreksi' => $value['nilai_terkoreksi'], //add db
            //         // 'nilai_negosiasi' => $value['nilai_negosiasi'], //add db
            //         'kontrak_nilai' => $value['total_harga'],
            //         'realisasi_pdn' => $value['total_harga'],
            //         'realisasi_umk' => $value['total_harga']
            //     ]);
            // }

        }

        // $data = DB::select(DB::raw("
        //         select a.tahun_anggaran, a.kd_klpd, a.satker_id, a.nama_satker ,a.alamat_satker ,a.npwp_satker ,a.nama_paket ,a.kd_rup,a.nama_sumber_dana ,a.kode_anggaran
        //         ,a.ppk_nip ,a.jabatan_ppk ,max(a.tanggal_buat_paket) as  tanggal_buat_paket, max(a.tanggal_edit_paket) as tanggal_edit_paket ,a.status_paket ,a.paket_status_str
        //         , STRING_AGG(a.no_paket, '/ ') as kd_paket, STRING_AGG(kd_penyedia, ' / ') as kd_penyedia, sum(a.pagu) as pagu, sum(a.realisasi) as realisasi from (
        //             select
        //             tahun_anggaran, kd_klpd, satker_id, nama_satker ,alamat_satker ,npwp_satker ,nama_paket ,kd_rup ,no_paket ,nama_sumber_dana ,kode_anggaran ,kd_penyedia ,sum(ep.total) as pagu
        //             ,sum(total_harga) as realisasi ,ppk_nip ,jabatan_ppk ,tanggal_buat_paket ,tanggal_edit_paket ,status_paket ,paket_status_str
        //             from ecatalog_paket ep
        //             where ep.paket_status_str = 'Paket Selesai'
        //             group by tahun_anggaran, tahun_anggaran, kd_klpd, satker_id, nama_satker ,alamat_satker ,npwp_satker ,nama_paket ,kd_rup ,no_paket ,nama_sumber_dana ,kode_anggaran ,kd_penyedia, kd_klpd,
        //             satker_id, nama_satker ,alamat_satker ,npwp_satker ,nama_paket ,kd_rup ,nama_sumber_dana ,kode_anggaran ,kd_penyedia ,ppk_nip ,jabatan_ppk,tanggal_buat_paket ,tanggal_edit_paket
        //             ,status_paket ,paket_status_str
        //         ) as a
        //         group by a.tahun_anggaran, a.kd_klpd, a.satker_id, a.nama_satker ,a.alamat_satker ,a.npwp_satker ,a.nama_paket ,a.kd_rup ,a.nama_sumber_dana ,a.kode_anggaran,a.ppk_nip ,a.jabatan_ppk ,a.status_paket ,a.paket_status_str
        //         "
        //       )
        //     );

        //     $filterEcatalog = json_encode($data);
        //     $arr = json_decode($filterEcatalog, true);
        //     foreach($arr as $resp => $value) {
        //             SpsePaket::updateOrCreate([
        //                 'ang_tahun' => $value['tahun_anggaran'],
        //                 'kd_klpd' => $value['kd_klpd'],  //add db
        //                 // 'nama_klpd' => $value['nama_klpd'], //add db not use
        //                 // 'jenis_klpd' => $value['jenis_klpd'], //add db not user
        //                 'idsatker' => $value['satker_id'],
        //                 // 'kd_satker_str' => $value['kd_satker_str'], //add db not use
        //                 'stk_nama' => $value['nama_satker'],
        //                 // 'kd_lpse' => $value['kd_lpse'], //add db not use
        //                 // 'nama_lpse' => $value['nama_lpse'], //add db
        //                 'idlelang' => $value['kd_paket'],
        //                 'kd_pkt_dce' => $value['kd_paket'], //add db
        //                 'rup_id' => $value['kd_rup'],
        //                 'pkt_nama' => $value['nama_paket'],
        //                 'pkt_pagu' => $value['pagu'],
        //                 // 'pkt_hps' => $value['hps'], not use
        //                 'sumber_dana' => $value['nama_sumber_dana'],
        //                 'mak' => $value['kode_anggaran'],
        //                 // 'kualifikasi_paket' => $value['kualifikasi_paket'], //add db
        //                 'mtd_pemilihan' => 'E-purchasing', //add db
        //                 // 'mtd_evaluasi' => $value['mtd_evaluasi'], //add db not use in non tender
        //                 // 'mtd_kualifikasi' => $value['mtd_kualifikasi'], //add db
        //                 // 'kontrak_pembayaran' => $value['kontrak_pembayaran'], //add db
        //                 'tahapan' => $value['paket_status_str'],
        //                 'lls_tgl_setuju' => $value['tanggal_buat_paket'],
        //                 'tgl_penetapan_pemenang' => $value['tanggal_edit_paket'],//add db
        //                 // 'penawaran' => $value['nilai_penawaran'],
        //                 // 'pemenang' => 'https://isb.lkpp.go.id/isb-2/api/784c6df9-7502-43bf-9a43-92ac79f5a44c/json/10252/Ecat-PenyediaDetail/tipe/4/parameter/'.$value['kd_penyedia'],
        //                 'pemenang' => $value['kd_penyedia'],
        //                 // 'nilai_terkoreksi' => $value['nilai_terkoreksi'], //add db
        //                 // 'nilai_negosiasi' => $value['nilai_negosiasi'], //add db
        //                 'kontrak_nilai' => $value['realisasi'],
        //                 'realisasi_pdn' => $value['realisasi'],
        //                 'realisasi_umk' => $value['realisasi']
        //             ])->where('kd_rup', $value['kd_rup']);
        //         }

        $this->output->progressFinish();
        Log::info("Ecatalog Cron execution!");
        $this->info('Ecatalog:Cron Command is working fine!');
    }
}
