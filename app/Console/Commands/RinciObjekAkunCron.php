<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SirupRinciObjekAkun;

class RinciObjekAkunCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rinciobjekakun:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rinci Objek Akun Command Execute Succesfull!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://isb.lkpp.go.id/isb/api/67960862-9068-44f8-99db-9c2255090d6c/json/201100555/RinciObjekAkunMasterRUP/tipe/4:12/parameter/2024:D95');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        foreach($program as $res => $value) {
            SirupRinciObjekAkun::updateOrCreate([
                'id_program' => $value['id_program'],
                'id_kegiatan' => $value['id_kegiatan'],
                'id_rinci_objek_akun' => $value['id'],
                'kode_rinciobjekakund' => $value['kode_rinciobjekakund'],
                'uraian_rinciobjekakun' => $value['uraian_rinciobjekakun'],
                'pagu' => $value['pagu'],
                'id_client' => $value['id_client'],
                'is_deleted' => $value['is_deleted']
            ]);
        }
        Log::info("RinciObjekAkun Cron execution!");
        $this->info('RinciObjekAkun:Cron Command is working fine!');
    }
}
