<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SirupProgram;

class ProgramCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'program:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Program Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://isb.lkpp.go.id/isb/api/18be00a8-f3df-418b-8ba4-eaf4b13af81e/json/141625419/ProgramMasterRUP/tipe/4:12/parameter/2024:D95');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        foreach($program as $res => $value) {
            SirupProgram::updateOrCreate([
                'id_sirup' => $value['id'],
                'kode_programs' => $value['kode_programs'],
                'nama' => $value['nama'],
                'pagu' => $value['pagu'],
                'id_client' => $value['id_client'],
                'is_deleted' => $value['is_deleted']
            ])->where('id_sirup', $value['id']);
        }
        Log::info("Program Cron execution!");
        $this->info('Program:Cron Command is working fine!');
    }
}
