<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SpsePaket;

class NonTenderConversion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nontender-all-conversion:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Non Tender ALL conversion Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Update Query......');
        $data = DB::select(DB::raw("
        select * from (
            select sp.idrup, sp.namapaket, sp.namasatker, sp.tahunanggaran, sp.jumlahpagu, np.status_nontender, sp.ppk, sp.statustkdn, sp.statususahakecil, sp.statusumumkan, sp.metodepengadaan, np.mtd_pemilihan, np.status_nontender, np.hps,
                np.tgl_buat_paket, np.tgl_pengumuman_nontender, max(ne.no_kontrak) as no_kontrak, max(ne.tgl_kontrak) as tgl_kontrak, ne.nama_penyedia,np.versi_nontender, max(ne.versi_addendum) as versi_addendum, sum(ne.nilai_kontrak) as kontrak, sum(ne.nilai_pdn_kontrak) as pdn, sum(ne.nilai_umk_kontrak) as umk
            from sirup_penyedia sp
                left join nontender_pengumuman np on np.kd_rup ILIKE '%' || sp.idrup || '%'
                left join nontender_selesai ns on ns.kd_nontender = np.kd_nontender
                left join nontender_ekontrak ne on ne.kd_nontender = np.kd_nontender
            where np.kd_klpd = 'D95' and sp.tahunanggaran = 2023 and sp.statusumumkan = 'Terumumkan'
                group by sp.idrup, sp.namapaket, sp.namasatker, sp.tahunanggaran, sp.jumlahpagu, np.status_nontender,
                sp.ppk, sp.statustkdn, sp.statususahakecil, sp.statusumumkan, sp.metodepengadaan, np.mtd_pemilihan, np.status_nontender, np.hps,
                np.tgl_buat_paket, np.tgl_pengumuman_nontender, ne.nama_penyedia,np.versi_nontender
            ) paketnontender
            where paketnontender.no_kontrak notnull
            "
            )
        );
        $this->info('Truncate lastest data......');
        DB::table('convert_nontender')->truncate();
        $this->info('Truncate Success!');
        $this->info('Update Data......');

            $tenderAllConversion = json_encode($data);
            $arr = json_decode($tenderAllConversion, true);
            $this->output->progressStart(count($arr));
            foreach($arr as $resp => $value) {
                $this->output->progressAdvance();
                    DB::table('convert_nontender')->insert([
                        'idrup' => $value['idrup'],
                        'namapaket' => $value['namapaket'],
                        'namasatker' => $value['namasatker'],
                        'tahunanggaran' => $value['tahunanggaran'],
                        'jumlahpagu' => $value['jumlahpagu'],
                        'status_nontender' => $value['status_nontender'],
                        'ppk' => $value['ppk'],
                        'statustkdn' => $value['statustkdn'],
                        'statususahakecil' => $value['statususahakecil'],
                        'statusumumkan' => $value['statusumumkan'],
                        'metodepengadaan' => $value['metodepengadaan'],
                        'mtd_pemilihan' => $value['mtd_pemilihan'],
                        'hps' => $value['hps'],
                        'tgl_buat_paket' => $value['tgl_buat_paket'],
                        'tgl_pengumuman_nontender' => $value['tgl_pengumuman_nontender'],
                        'no_kontrak' => $value['no_kontrak'],
                        'tgl_kontrak' => $value['tgl_kontrak'],
                        'nama_penyedia' => $value['nama_penyedia'],
                        'versi_nontender' => $value['versi_nontender'],
                        'versi_addendum' => $value['versi_addendum'],
                        'kontrak' => $value['kontrak'],
                        'pdn' => $value['pdn'],
                        'umk' => $value['umk']
                    ]);
                }

        $this->output->progressFinish();

        // ================ Tender selesai insert ke table spse paket ======

        $QueryTenderSelesai = DB::select(DB::raw("
            select * from (
                select sp.idrup, sp.namapaket, sp.namasatker, sp.tahunanggaran, sp.jumlahpagu, np.status_nontender, sp.ppk, sp.statustkdn, sp.statususahakecil, sp.statusumumkan, sp.metodepengadaan, np.mtd_pemilihan, np.status_nontender, np.hps,
                    np.tgl_buat_paket, np.tgl_pengumuman_nontender, max(ne.no_kontrak) as no_kontrak, max(ne.tgl_kontrak) as tgl_kontrak, ne.nama_penyedia,np.versi_nontender, max(ne.versi_addendum) as versi_addendum, sum(ne.nilai_kontrak) as kontrak, sum(ne.nilai_pdn_kontrak) as pdn, sum(ne.nilai_umk_kontrak) as umk
                from sirup_penyedia sp
                    left join nontender_pengumuman np on np.kd_rup ILIKE '%' || sp.idrup || '%'
                    left join nontender_selesai ns on ns.kd_nontender = np.kd_nontender
                    left join nontender_ekontrak ne on ne.kd_nontender = np.kd_nontender
                where np.kd_klpd = 'D95' and sp.tahunanggaran = 2023 and np.status_nontender = 'Selesai' and sp.statusumumkan = 'Terumumkan'
                    group by sp.idrup, sp.namapaket, sp.namasatker, sp.tahunanggaran, sp.jumlahpagu, np.status_nontender,
                    sp.ppk, sp.statustkdn, sp.statususahakecil, sp.statusumumkan, sp.metodepengadaan, np.mtd_pemilihan, np.status_nontender, np.hps,
                    np.tgl_buat_paket, np.tgl_pengumuman_nontender, ne.nama_penyedia,np.versi_nontender
                ) paketnontender
                where paketnontender.no_kontrak notnull
            "
            )
        );
        $this->info('Delete old data epurchasing selesai......');
        DB::table('spse_paket')
        ->where('mtd_pemilihan', 'Pengadaan Langsung')
        ->orWhere('mtd_pemilihan', 'Penunjukan Langsung')
        ->delete();
        $this->info('Delete Success!');
        $this->info('Insert new Data......');

            $tenderSelesai = json_encode($QueryTenderSelesai);
            $array = json_decode($tenderSelesai, true);
            $this->output->progressStart(count($array));
            foreach($array as $resp => $value) {
                $this->output->progressAdvance();
                    DB::table('spse_paket')->insert([
                        'ang_tahun' => $value['tahunanggaran'],
                        'rup_id' => $value['idrup'],
                        'pkt_nama' => $value['namapaket'],
                        'stk_nama' => $value['namasatker'],
                        'pkt_pagu' => $value['jumlahpagu'],
                        'nama_ppk' => $value['ppk'],
                        'statustkdn' => $value['statustkdn'],
                        'statususahakecil' => $value['statususahakecil'],
                        'statusumumkan' => $value['statusumumkan'],
                        'mtd_pemilihan' => $value['mtd_pemilihan'],
                        'tahapan' => $value['status_nontender'],
                        'pkt_hps' => $value['hps'],
                        'lls_dibuat_tanggal' => $value['tgl_buat_paket'],
                        'lls_tgl_setuju' => $value['tgl_pengumuman_nontender'],
                        'kontrak_no' => $value['no_kontrak'],
                        'tgl_kontrak' => $value['tgl_kontrak'],
                        'pemenang' => $value['nama_penyedia'],
                        'kontrak_nilai' => $value['kontrak'],
                        'realisasi_pdn' => $value['pdn'],
                        'realisasi_umk' => $value['umk']
                    ]);
                }

        $this->output->progressFinish();
        Log::info("Tender All Cron execution!");
        $this->info('Tender All:Cron Command is working fine!');
    }
}
