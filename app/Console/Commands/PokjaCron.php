<?php

namespace App\Console\Commands;

use App\Models\AnggotaPokja;
use Illuminate\Console\Command;
use DB;
use App\Models\Pokja;
use App\Models\NamaPokja;
use Illuminate\Support\Facades\Log;

class PokjaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pokja:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Input Pokja Dari SPSE';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        // $request = $client->get(env("URL_API_SPSE").'/api/v1/siukpbj/nama-pokja?tahun=2021');
        $request = $client->get('https://isb.lkpp.go.id/isb/api/469e3eae-25e1-47dc-9e52-17b2b563baa7/json/210914541/PokjaPerTenderSPSE/tipe/4:4/parameter/2023:14');
        $response = $request->getBody()->getContents();
        $pokja = json_decode($response, true);
        foreach($pokja as $res => $value) {
            // print_r($value);
            Pokja::updateOrCreate([
                'pnt_id' => $value['username'],
                'nama_pokja' => $value['nama_pokja'],
                'user_id' => 0,
                'pnt_no_sk' => $value['no_sk_pokja'],
                'pnt_tahun' => $value['tahun_pokja'],
                'pnt_email' => $value['email_pegawai'],
                'stk_nama' => $value['nama_satker'],
                'nama_pegawai' => $value['nama_pegawai'],
                'nip_pegawai'=> $value['nip_pegawai'],
                'tahun_anggaran'=> $value['tahun_anggaran'],
                'kd_tender'=> $value['kd_tender'],
                'kd_lpse'=> $value['kd_lpse'],
                'kd_klpd'=> $value['kd_klpd'],
                'nama_klpd'=> $value['nama_klpd'],
                'kd_satker'=> $value['kd_satker'],
                'kd_satker_str'=> $value['kd_satker_str']
            ])->where('pnt_id', $value['username']);

            NamaPokja::updateOrCreate([
                'nama_pokja' => $value['nama_pokja'],
                'tahun_anggaran'=> $value['tahun_anggaran'],
                'kd_klpd'=> $value['kd_klpd'],
                'nama_klpd'=> $value['nama_klpd']
            ])->where('nama_pokja', $value['nama_pokja']);

            AnggotaPokja::updateOrCreate([
                // 'username_pokja' => $value['username'], hide karna tidak valid
                'nama_pokja' => $value['nama_pokja'],
                'no_sk_pokja' => $value['no_sk_pokja'],
                'tahun_pokja' => $value['tahun_pokja'],
                'email_pegawai' => $value['email_pegawai'],
                'nama_satker' => $value['nama_satker'],
                'nama_pegawai' => $value['nama_pegawai'],
                'nip_pegawai'=> $value['nip_pegawai'],
                'tahun_anggaran'=> $value['tahun_anggaran']
            ])->where('nama_pegawai', $value['nama_pegawai']);
        }

        Log::info("Pokja Cron execution!");
        $this->info('Pokja:Cron Command is working fine!');
    }
}
