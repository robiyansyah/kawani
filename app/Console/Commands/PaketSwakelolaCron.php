<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SirupPaketSwakelola;

class PaketSwakelolaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paketswakelola:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Paket Swakelola Command Execute Succesfull!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/cab90506-bb34-4f61-b2df-71580f8729f4/json/2827/RUP-PaketSwakelola-Terumumkan/tipe/4:12/parameter/2024:D95');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);

        $this->info('Truncate Data RUP Swakelola');
        SirupPaketSwakelola::where('tahunanggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data RUP Swakelola');
        $this->output->progressStart(count($program));
        foreach($program as $res => $value) {
            $this->output->progressAdvance();
            SirupPaketSwakelola::insert([
                'klpd' => $value['nama_klpd'],
                'tahunanggaran' => $value['tahun_anggaran'],
                'idrup' => $value['kd_rup'],
                'namapaket' => $value['nama_paket'],
                'jumlahpagu' => $value['pagu'],
                'namasatker' => $value['nama_satker'],
                'kodesatker' => $value['kd_satker'],
                // 'lokasi' => $value['lokasi'],
                'tanggalawalpekerjaan' => $value['tgl_awal_pelaksanaan_kontrak'],
                'tanggalakhirpekerjaan' => $value['tgl_akhir_pelaksanaan_kontrak'],
                'keterangan' => $value['uraian_pekerjaan'],
                'ppk' => $value['nama_ppk'],
                'nip_ppk' => $value['nip_ppk'],
                'tanggalpengumuman' => $value['tgl_pengumuman_paket'],
                'statusdeletepaket' => $value['status_delete_rup'],
                'statusaktifpaket' => $value['status_aktif_rup'],
                'statusumumkan' => $value['status_umumkan_rup'],
                'id_rup_client' => $value['kd_rup_lokal'],
                'tipe_swakelola' => $value['tipe_swakelola'],
                // 'kode_klpd_penyelenggara' => $value['kode_klpd_penyelenggara'],
                // 'nama_klpd_penyelenggara' => $value['nama_klpd_penyelenggara'],
                // 'nama_satker_penyelenggara' => $value['nama_satker_penyelenggara'],
                'username' => $value['username_ppk'],
                'kd_klpd' => $value['kd_klpd'],
                'volume' => $value['volume_pekerjaan'],
                // 'lokasi_kab' => $value['lokasi_kab'],
                // 'lokasi_prov' => $value['lokasi_prov'],
                // 'idsatker' => $value['idsatker']
            ]);
        }
        $this->output->progressFinish();
        Log::info("SirupPaketSwakelola Cron execution!");
        $this->info('SirupPaketSwakelola:Cron Command is working fine!');
    }
}
