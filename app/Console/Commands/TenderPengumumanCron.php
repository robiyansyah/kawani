<?php

namespace App\Console\Commands;

use App\Models\TenderPengumuman;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use SplHeap;

class TenderPengumumanCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenderpengumuman:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tender Pengumuman Cron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/2ee2a69f-4bb3-4483-9197-56e8fd1a8261/json/2821/SPSE-TenderPengumuman/tipe/4:4/parameter/2024:14');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        $this->info('Truncate Data ......');
        TenderPengumuman::where('tahun_anggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data ......');
        $this->output->progressStart(count($program));
        foreach($program as $res => $value) {
            $this->output->progressAdvance();
            TenderPengumuman::insert([
                'tahun_anggaran' => $value['tahun_anggaran'],
                'kd_klpd' => $value['kd_klpd'],
                'nama_klpd' => $value['nama_klpd'],
                'jenis_klpd' => $value['jenis_klpd'],
                'kd_satker' => $value['kd_satker'],
                'kd_satker_str' => $value['kd_satker_str'],
                'nama_satker' => $value['nama_satker'],
                'kd_lpse' => $value['kd_lpse'],
                'nama_lpse' => $value['nama_lpse'],
                'kd_tender' => $value['kd_tender'],
                'kd_pkt_dce' => $value['kd_pkt_dce'],
                'kd_rup' => $value['kd_rup'],
                'nama_paket' => $value['nama_paket'],
                'pagu' => $value['pagu'],
                'hps' => $value['hps'],
                'sumber_dana' => $value['sumber_dana'],
                'kualifikasi_paket' => $value['kualifikasi_paket'],
                'jenis_pengadaan' => $value['jenis_pengadaan'],
                'mtd_pemilihan' => $value['mtd_pemilihan'],
                'mtd_evaluasi' => $value['mtd_evaluasi'],
                'mtd_kualifikasi' => $value['mtd_kualifikasi'],
                'kontrak_pembayaran' => $value['kontrak_pembayaran'],
                'status_tender' => $value['status_tender'],
                'tanggal_status' => $value['tanggal_status'],
                'versi_tender' => $value['versi_tender'],
                'ket_ditutup' => $value['ket_ditutup'],
                'ket_diulang' => $value['ket_diulang'],
                'tgl_buat_paket' => $value['tgl_buat_paket'],
                'tgl_kolektif_kolegial' => $value['tgl_kolektif_kolegial'],
                'tgl_pengumuman_tender' => $value['tgl_pengumuman_tender'],
                'nip_ppk' => $value['nip_ppk'],
                'nama_ppk' => $value['nama_ppk'],
                'nip_pokja' => $value['nip_pokja'],
                'nama_pokja' => $value['nama_pokja'],
                'lokasi_pekerjaan' => $value['lokasi_pekerjaan'],
                'url_lpse' => $value['url_lpse']
            ]);
        }
        $this->output->progressFinish();
        Log::info("Tender Pengumuman Cron execution!");
        $this->info('TenderPengumuman:Cron Command is working fine!');
    }
}
