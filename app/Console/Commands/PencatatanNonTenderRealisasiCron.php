<?php

namespace App\Console\Commands;

use App\Models\PencatatanNontenderSelesai;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SpsePaket;

class PencatatanNonTenderRealisasiCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pencatatan-nontender-realisasi:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pencatatan Non Tender Realisasi Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Cek url......');
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/48fbd32d-4706-4d64-8a0b-231cb3e25f50/json/11607/SPSE-PencatatanNonTenderRealisasi/tipe/4:4/parameter/2024:14');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        $this->info('Truncate Data ......');
        PencatatanNontenderSelesai::where('tahun_anggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data ......');
        $this->output->progressStart(count($program));
        foreach($program as $res => $value) {
            $this->output->progressAdvance();
            PencatatanNontenderSelesai::insert([
                'tahun_anggaran' => $value['tahun_anggaran'],
                'kd_klpd' => $value['kd_klpd'],
                'kd_satker' => $value['kd_satker'],
                'kd_lpse' => $value['kd_lpse'],
                'kd_nontender_pct' => $value['kd_nontender_pct'],
                'kategori_pengadaan' => $value['kategori_pengadaan'],
                'mtd_pemilihan' => $value['mtd_pemilihan'],
                'jenis_realisasi' => $value['jenis_realisasi'],
                'no_realisasi' => $value['no_realisasi'],
                'tgl_realisasi' => $value['tgl_realisasi'],
                'nilai_realisasi' => $value['nilai_realisasi'],
                'dok_realisasi' => $value['dok_realisasi'],
                'ket_realisasi' => $value['ket_realisasi'],
                'nama_penyedia' => $value['nama_penyedia'],
                'npwp_penyedia' => $value['npwp_penyedia'],
                'nip_ppk' => $value['nip_ppk'],
                'nama_ppk' => $value['nama_ppk']
            ]);
        }

        Log::info("Pencatatan Non Tender Realisasi Cron execution!");
        $this->info('Pencatatan Non Tender Realisasi:Cron Command is working fine!');

    }
}
