<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SpsePaket;

class EpurchasingConversion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'epurchasing-all-conversion:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Epurchasing ALL conversion Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Update Query......');
        $data = DB::select(DB::raw("
            select case when sp.kd_klpd is null then ep.klpd else sp.kd_klpd end klpd,
                case when sp.tahunanggaran is null then ep.tahun_anggaran else sp.tahunanggaran end tahunanggaran,
                case when sp.idrup is null then ep.kd_rup else sp.idrup end idrup,
                case when sp.namapaket is null then ep.nama_paket else sp.namapaket end nama_paket,
                ss.namasatker, sp.metodepengadaan,
                sp.jumlahpagu, sp.ppk, sp.statustkdn, sp.statususahakecil, sp.statusumumkan,
                ep.kd_penyedia, ep.total_harga Total_Epurchasing, ep.paket_status_str Status_Epurchasing
            from sirup_penyedia sp
            full outer join
                (
                    select ep.kd_klpd klpd, ep.tahun_anggaran, ep.kd_rup, ep.nama_paket, ep.satker_id, ep.kd_penyedia, Sum(ep.total_harga) total_harga, ep.paket_status_str
                    from epurchasing_paket ep
                    group by ep.kd_klpd, ep.tahun_anggaran, ep.kd_rup, ep.kd_penyedia, ep.nama_paket, ep.satker_id, ep.paket_status_str
                ) ep on  sp.kd_klpd = ep.klpd and sp.tahunanggaran = ep.tahun_anggaran and sp.idrup  = ep.kd_rup
                left join satker_sirup ss on case when sp.kodesatker is null then ep.satker_id else sp.kodesatker end = ss.kodesatker
                where case when  sp.kd_klpd is null then ep.klpd else sp.kd_klpd end = 'D95' and (sp.metodepengadaan = 'e-Purchasing' or sp.metodepengadaan is null)
                ;
            "
            )
        );
        $this->info('Truncate lastest data......');
        DB::table('convert_epurchasing')->truncate();
        $this->info('Truncate Success!');
        $this->info('Update Data......');

            $tenderAllConversion = json_encode($data);
            $arr = json_decode($tenderAllConversion, true);
            $this->output->progressStart(count($arr));
            foreach($arr as $resp => $value) {
                $this->output->progressAdvance();
                    DB::table('convert_epurchasing')->insert([
                        'klpd' => $value['klpd'],
                        'tahunanggaran' => $value['tahunanggaran'],
                        'idrup' => $value['idrup'],
                        'nama_paket' => $value['nama_paket'],
                        'namasatker' => $value['namasatker'],
                        'metodepengadaan' => $value['metodepengadaan'],
                        'jumlahpagu' => $value['jumlahpagu'],
                        'ppk' => $value['ppk'],
                        'statustkdn' => $value['statustkdn'],
                        'statususahakecil' => $value['statususahakecil'],
                        'statusumumkan' => $value['statusumumkan'],
                        'kd_penyedia' => $value['kd_penyedia'],
                        'total_epurchasing' => $value['total_epurchasing'],
                        'status_epurchasing' => $value['status_epurchasing']
                    ]);
                }

        $this->output->progressFinish();

        // ================ Tender selesai insert ke table spse paket ======

        $QueryTenderSelesai = DB::select(DB::raw("
            select case when sp.kd_klpd is null then ep.klpd else sp.kd_klpd end klpd,
                case when sp.tahunanggaran is null then ep.tahun_anggaran else sp.tahunanggaran end tahunanggaran,
                case when sp.idrup is null then ep.kd_rup else sp.idrup end idrup,
                case when sp.namapaket is null then ep.nama_paket else sp.namapaket end nama_paket,
                ss.namasatker, sp.metodepengadaan,
                sp.jumlahpagu, sp.ppk, sp.statustkdn, sp.statususahakecil, sp.statusumumkan,
                ep.kd_penyedia, ep.total_harga Total_Epurchasing, ep.paket_status_str Status_Epurchasing
            from sirup_penyedia sp
            full outer join
                (
                    select ep.kd_klpd klpd, ep.tahun_anggaran, ep.kd_rup, ep.nama_paket, ep.satker_id, ep.kd_penyedia, Sum(ep.total_harga) total_harga, ep.paket_status_str
                    from epurchasing_paket ep
                    group by ep.kd_klpd, ep.tahun_anggaran, ep.kd_rup, ep.kd_penyedia, ep.nama_paket, ep.satker_id, ep.paket_status_str
                ) ep on  sp.kd_klpd = ep.klpd and sp.tahunanggaran = ep.tahun_anggaran and sp.idrup  = ep.kd_rup
                left join satker_sirup ss on case when sp.kodesatker is null then ep.satker_id else sp.kodesatker end = ss.kodesatker
                where ep.paket_status_str = 'Paket Selesai' and case when  sp.kd_klpd is null then ep.klpd else sp.kd_klpd end = 'D95' and (sp.metodepengadaan = 'e-Purchasing' or sp.metodepengadaan is null)
                ;
            "
            )
        );
        $this->info('Delete old data epurchasing selesai......');
        DB::table('spse_paket')
        ->where('mtd_pemilihan', 'e-Purchasing')
        ->delete();
        $this->info('Delete Success!');
        $this->info('Insert new Data......');

            $tenderSelesai = json_encode($QueryTenderSelesai);
            $array = json_decode($tenderSelesai, true);
            $this->output->progressStart(count($array));
            foreach($array as $resp => $value) {
                $this->output->progressAdvance();
                    DB::table('spse_paket')->insert([
                        'ang_tahun' => $value['tahunanggaran'],
                        'rup_id' => $value['idrup'],
                        'pkt_nama' => $value['nama_paket'],
                        'stk_nama' => $value['namasatker'],
                        'pkt_pagu' => $value['jumlahpagu'],
                        'nama_ppk' => $value['ppk'],
                        'statustkdn' => $value['statustkdn'],
                        'statususahakecil' => $value['statususahakecil'],
                        'statusumumkan' => $value['statusumumkan'],
                        'mtd_pemilihan' => 'e-Purchasing',
                        'tahapan' => $value['status_epurchasing'],
                        // 'pkt_hps' => $value['hps'],
                        // 'lls_dibuat_tanggal' => $value['tgl_buat_paket'],
                        // 'lls_tgl_setuju' => $value['tgl_pengumuman_tender'],
                        // 'kontrak_no' => $value['no_kontrak'],
                        // 'tgl_kontrak' => $value['tgl_kontrak'],
                        'pemenang' => $value['kd_penyedia'],
                        'kontrak_nilai' => $value['total_epurchasing'],
                        'realisasi_pdn' => $value['total_epurchasing'],
                        'realisasi_umk' => $value['total_epurchasing']
                    ]);
                }

        $this->output->progressFinish();
        Log::info("Tender All Cron execution!");
        $this->info('Tender All:Cron Command is working fine!');
    }
}
