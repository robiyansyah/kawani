<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SpsePaket;

class TokoDaringConversion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'toko-daring-conversion:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Toko Daring ALL conversion Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Update Query......');
        $data = DB::select(DB::raw("
            select td.kd_klpd, td.nama_klpd , td.kd_satker , td.nama_satker, td.order_id , td.order_desc , td.valuasi , td.kategori , td.metode_bayar , td.tanggal_transaksi , td.marketplace , td.nama_merchant ,
                td. jenis_transaksi , td.kota_kab , td.provinsi , td.nama_pemesan , td. status_verif , td.sumber_data , td.status_konfirmasi_ppmse , rt.id_rup
                from toko_daring td
                left join rup_tokodaring rt on rt.order_id = td.order_id
                left join satker_sirup ss on ss.kodesatker = td.kd_satker
            where td.kd_klpd = 'D95' and td.status_konfirmasi_ppmse notnull
            "
            )
        );
        $this->info('Truncate lastest data......');
        DB::table('convert_tokodaring')->truncate();
        $this->info('Truncate Success!');
        $this->info('Update Data......');

            $tenderAllConversion = json_encode($data);
            $arr = json_decode($tenderAllConversion, true);
            $this->output->progressStart(count($arr));
            foreach($arr as $resp => $value) {
                $this->output->progressAdvance();
                    DB::table('convert_tokodaring')->insert([
                        'kd_klpd' => $value['kd_klpd'],
                        'nama_klpd' => $value['nama_klpd'],
                        'kd_satker' => $value['kd_satker'],
                        'nama_satker' => $value['nama_satker'],
                        'order_id' => $value['order_id'],
                        'order_desc' => $value['order_desc'],
                        'valuasi' => $value['valuasi'],
                        'kategori' => $value['kategori'],
                        'metode_bayar' => $value['metode_bayar'],
                        'tanggal_transaksi' => $value['tanggal_transaksi'],
                        'marketplace' => $value['marketplace'],
                        'nama_merchant' => $value['nama_merchant'],
                        'jenis_transaksi' => $value['jenis_transaksi'],
                        'kota_kab' => $value['kota_kab'],
                        'provinsi' => $value['provinsi'],
                        'nama_pemesan' => $value['nama_pemesan'],
                        'status_verif' => $value['status_verif'],
                        'sumber_data' => $value['sumber_data'],
                        'status_konfirmasi_ppmse' => $value['status_konfirmasi_ppmse'],
                        'id_rup' => $value['id_rup']
                    ]);
                }

        $this->output->progressFinish();

        // ================ Tender selesai insert ke table spse paket ======

        $QueryTenderSelesai = DB::select(DB::raw("
            select td.kd_klpd, td.nama_klpd , td.kd_satker , td.nama_satker, td.order_id , td.order_desc , td.valuasi , td.kategori , td.metode_bayar , td.tanggal_transaksi , td.marketplace , td.nama_merchant ,
                td. jenis_transaksi , td.kota_kab , td.provinsi , td.nama_pemesan , td. status_verif , td.sumber_data , td.status_konfirmasi_ppmse , rt.id_rup
            from toko_daring td
                left join rup_tokodaring rt on rt.order_id = td.order_id
                left join satker_sirup ss on ss.kodesatker = td.kd_satker
            where td.kd_klpd = 'D95' and td.status_konfirmasi_ppmse = 'selesai' and td.status_verif  = 'verified' and rt.id_rup != null            "
            )
        );
        $this->info('Delete old data toko daring selesai......');
        DB::table('spse_paket')
        ->where('kgr_id', 'Toko Daring')
        ->delete();
        $this->info('Delete Success!');
        $this->info('Insert new Data......');

            $tenderSelesai = json_encode($QueryTenderSelesai);
            $array = json_decode($tenderSelesai, true);
            $this->output->progressStart(count($array));
            foreach($array as $resp => $value) {
                $this->output->progressAdvance();
                    DB::table('spse_paket')->insert([
                        'ang_tahun' => date('Y', strtotime($value['tanggal_transaksi'])),
                        'rup_id' => $value['idrup'],
                        'pkt_nama' => $value['order_desc'],
                        'stk_nama' => $value['nama_satker'],
                        // 'pkt_pagu' => $value['pagu'],
                        'nama_ppk' => $value['nama_pemesan'],
                        'kgr_id' => 'Toko Daring',
                        // 'statustkdn' => $value['statustkdn'],
                        // 'statususahakecil' => $value['statususahakecil'],
                        // 'statusumumkan' => $value['statusumumkan'],
                        'mtd_pemilihan' => 'e-Purchasing',
                        'idlelang' => $value['order_id'],
                        'tahapan' => $value['status_konfirmasi_ppmse'],
                        // 'lls_versi_lelang' => $value['versi_tender'],
                        'pkt_hps' => 0,
                        'lls_dibuat_tanggal' => $value['tanggal_transaksi'],
                        'lls_tgl_setuju' => $value['tanggal_transaksi'],
                        'kontrak_no' => $value['order_id'],
                        'tgl_kontrak' => $value['tanggal_transaksi'],
                        'pemenang' => $value['nama_merchant'],
                        'kontrak_nilai' => $value['valuasi'],
                        'realisasi_pdn' => $value['valuasi'],
                        'realisasi_umk' => $value['valuasi']
                    ]);
                }

        $this->output->progressFinish();
        Log::info("Toko Daring All Cron execution!");
        $this->info('Toko Daring All:Cron Command is working fine!');
    }
}
