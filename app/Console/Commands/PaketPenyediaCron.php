<?php

namespace App\Console\Commands;

use App\Models\SatkerSirup;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SirupPaketPenyedia;

class PaketPenyediaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paketpenyedia:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Paket Penyedia Command Execute Succesfull!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Cek url......');
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/eda9c561-858e-4b14-aa48-844a017cf4e6/json/2824/RUP-PaketPenyedia-Terumumkan/tipe/4:12/parameter/2024:D95');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        SirupPaketPenyedia::where('tahunanggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data ......');
        $this->output->progressStart(count($program));
        foreach($program as $res => $value) {
            $this->output->progressAdvance();
            SirupPaketPenyedia::insert([
                'tahunanggaran' => $value['tahun_anggaran'],
                'kd_klpd' => $value['kd_klpd'],
                'klpd' => $value['nama_klpd'],
                'jenis_klpd' => $value['jenis_klpd'], //note add db
                'kodesatker' => $value['kd_satker'],
                'idsatker' => $value['kd_satker_str'],
                'namasatker' => $value['nama_satker'],
                'idrup' => $value['kd_rup'],
                'namapaket' => $value['nama_paket'],
                'jumlahpagu' => $value['pagu'],
                'idmetodepengadaan' => $value['kd_metode_pengadaan'],
                'metodepengadaan' => $value['metode_pengadaan'],
                'idjenispengadaan' => $value['kd_jenis_pengadaan'],
                'statuspradipa' => $value['status_pradipa'],
                'statustkdn' => $value['status_pdn'],
                'statususahakecil' => $value['status_ukm'],
                'alasan_non_ukm' => $value['alasan_non_ukm'], // note add db
                'statuspaketkonsolidasi' => $value['status_konsolidasi'],
                'statuspenyedia' => $value['tipe_paket'],
                'id_swakelola' => $value['kd_rup_swakelola'],
                'id_rup_client' => $value['kd_rup_lokal'],
                'volume' => $value['volume_pekerjaan'],
                'urarian_pekerjaan' => $value['urarian_pekerjaan'], // note add db
                'spesifikasi' => $value['spesifikasi_pekerjaan'],
                'tanggalawalpemilihan' => $value['tgl_awal_pemilihan'],
                'tanggalakhirpemilihan' => $value['tgl_akhir_pemilihan'],
                'tanggalawalpekerjaan' => $value['tgl_awal_kontrak'],
                'tanggalakhirpekerjaan' => $value['tgl_akhir_kontrak'],
                'tgl_awal_pemanfaatan' => $value['tgl_awal_pemanfaatan'], // note add db
                'tgl_akhir_pemanfaatan' => $value['tgl_akhir_pemanfaatan'], // note add db
                'tgl_buat_paket' => $value['tgl_buat_paket'], // note add db
                'tanggalpengumuman' => $value['tgl_pengumuman_paket'],
                'nip_ppk' => $value['nip_ppk'],
                'ppk' => $value['nama_ppk'],
                'username_ppk' => $value['username_ppk'], // note add db
                'statusaktifpaket' => $value['status_aktif_rup'],
                'statusdeletepaket' => $value['status_delete_rup'],
                'statusumumkan' => $value['status_umumkan_rup']

                // Delete field db
                // 'jenispengadaan' => $value['jenis_pengadaan'],
                // 'lokasi' => $value['lokasi'],
                // 'tanggalkebutuhan' => $value['tanggalkebutuhan'],
                // 'statuspenyedia' => $value['statuspenyedia'],
                // 'keterangan' => $value['keterangan'],
                // 'username' => $value['username'],
                // 'nip_ppk' => $value['nip_ppk'],
                // 'lokasi_kab' => $value['lokasi_kab'],
                // 'lokasi_prov' => $value['lokasi_prov'],
            ]);

            SatkerSirup::updateOrCreate([
                'namasatker' => $value['nama_satker'],
                'kodesatker' => $value['kd_satker']
            ])->where('namasatker', $value['nama_satker']);

        }
        $this->output->progressFinish();
        Log::info("SirupPaketPenyedia Cron execution!");
        $this->info('SirupPaketPenyedia:Cron Command is working fine!');
    }
}
