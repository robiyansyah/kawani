<?php

namespace App\Console\Commands;

use App\Models\TokoDaring;
use App\Models\SpsePaket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TokoDaringCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tokodaring:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Toko Daring Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $this->info('Checking url......');
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/2aeb570d-1166-46c3-948a-43b846704076/json/3066/Bela-TokoDaringRealisasi/tipe/12:4/parameter/D95:2024');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        TokoDaring::where('tanggal_transaksi','>', '31-12-2023')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data ......');
        $this->output->progressStart(count($program));
        foreach($program as $res => $value) {
            $this->output->progressAdvance();
            TokoDaring::insert([
                'kd_klpd' => $value['kd_klpd'],
                'nama_klpd' => $value['nama_klpd'],
                'kd_satker' => $value['kd_satker'],
                'nama_satker' => $value['nama_satker'],
                'order_id' => $value['order_id'],
                'order_desc' => $value['order_desc'],
                'valuasi' => $value['valuasi'],
                'kategori' => $value['kategori'],
                'metode_bayar' => $value['metode_bayar'],
                'tanggal_transaksi' => $value['tanggal_transaksi'],
                'marketplace' => $value['marketplace'],
                'nama_merchant' => $value['nama_merchant'],
                'jenis_transaksi' => $value['jenis_transaksi'],
                'kota_kab' => $value['kota_kab'],
                'provinsi' => $value['provinsi'],
                'nama_pemesan' => $value['nama_pemesan'],
                'status_verif' => $value['status_verif'],
                'sumber_data' => $value['sumber_data'],
                'status_konfirmasi_ppmse' => $value['status_konfirmasi_ppmse'],
                'keterangan_ppmse' => $value['keterangan_ppmse']
            ]);

        }
        $this->output->progressFinish();
        Log::info("Toko Daring Cron execution!");
        $this->info('Toko Daring:Cron Command is working fine!');
    }
}
