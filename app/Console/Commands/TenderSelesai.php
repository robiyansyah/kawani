<?php

namespace App\Console\Commands;

use App\Models\Tender;
use App\Models\SpsePaket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TenderSelesai extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenderselesai:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tender Command Executed Successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://isb.lkpp.go.id/isb-2/api/f6202f50-0a35-4270-8963-968b1a14c0d8/json/2829/SPSE-TenderSelesai/tipe/4:4/parameter/2024:14');
        $response = $request->getBody()->getContents();
        $dataTenderSelesai = json_decode($response, true);

        $this->info('Truncate Data ......');
        Tender::where('tahun_anggaran','2024')->delete();
        $this->info('Truncate Data Sukses......');
        $this->info('Update Data ......');
        $this->output->progressStart(count($dataTenderSelesai));
        foreach($dataTenderSelesai as $res => $value) {
            $this->output->progressAdvance();
            Tender::insert([
                'tahun_anggaran' => $value['tahun_anggaran'],
                'kd_klpd' => $value['kd_klpd'],
                'nama_klpd' => $value['nama_klpd'],
                'jenis_klpd' => $value['jenis_klpd'],
                'kd_satker' => $value['kd_satker'],
                'kd_satker_str' => $value['kd_satker_str'],
                'nama_satker' => $value['nama_satker'],
                'kd_lpse' => $value['kd_lpse'],
                'nama_lpse' => $value['nama_lpse'],
                'kd_tender' => $value['kd_tender'],
                'kd_pkt_dce' => $value['kd_pkt_dce'],
                'kd_rup' => $value['kd_rup'],
                'nama_paket' => $value['nama_paket'],
                'pagu' => $value['pagu'],
                'hps' => $value['hps'],
                'sumber_dana' => $value['sumber_dana'],
                'mak' => $value['mak'],
                'kualifikasi_paket' => $value['kualifikasi_paket'],
                'jenis_pengadaan' => $value['jenis_pengadaan'],
                'mtd_pemilihan' => $value['mtd_pemilihan'],
                'mtd_evaluasi' => $value['mtd_evaluasi'],
                'mtd_kualifikasi' => $value['mtd_kualifikasi'],
                'kontrak_pembayaran' => $value['kontrak_pembayaran'],
                'status_tender' => $value['status_tender'],
                'tgl_pengumuman_tender' => $value['tgl_pengumuman_tender'],
                'tgl_penetapan_pemenang' => $value['tgl_penetapan_pemenang'],
                'url_lpse' => $value['url_lpse']
            ]);
        }

        // $req = $client->get('https://dce.lkpp.go.id/isb-2/api/f72653f5-9cd4-4c47-aaac-9a96c923deb2/json/10297/SPSE-TenderSelesaiNilai/tipe/4:4/parameter/2023:14');
        // $resp = $req->getBody()->getContents();
        // $nilaiTender = json_decode($resp, true);
        // foreach($dataTenderSelesai as $res => $value) {
        //     SpsePaket::updateOrCreate([
        //         'ang_tahun' => $value['tahun_anggaran'],
        //         'kd_klpd' => $value['kd_klpd'],  //add db
        //         'nama_klpd' => $value['nama_klpd'], //add db
        //         'jenis_klpd' => $value['jenis_klpd'], //add db
        //         'idsatker' => $value['kd_satker'],
        //         'kd_satker_str' => $value['kd_satker_str'], //add db
        //         'stk_nama' => $value['nama_satker'],
        //         'kd_lpse' => $value['kd_lpse'], //add db
        //         'nama_lpse' => $value['nama_lpse'], //add db
        //         'idlelang' => $value['kd_tender'],
        //         'kd_pkt_dce' => $value['kd_pkt_dce'], //add db
        //         'rup_id' => $value['kd_rup'],
        //         'pkt_nama' => $value['nama_paket'],
        //         'pkt_pagu' => $value['pagu'],
        //         'pkt_hps' => $value['hps'],
        //         'sumber_dana' => $value['sumber_dana'], //add db
        //         'mak' => $value['mak'], //add db
        //         'kualifikasi_paket' => $value['kualifikasi_paket'], //add db
        //         'mtd_pemilihan' => $value['mtd_pemilihan'], //add db
        //         'mtd_evaluasi' => $value['mtd_evaluasi'], //add db
        //         'mtd_kualifikasi' => $value['mtd_kualifikasi'], //add db
        //         'kontrak_pembayaran' => $value['kontrak_pembayaran'], //add db
        //         'tahapan' => $value['status_tender'],
        //         'lls_tgl_setuju' => $value['tgl_pengumuman_tender'],
        //         'tgl_penetapan_pemenang' => $value['tgl_penetapan_pemenang']//add db

        //         // 'penawaran' => $value['nilai_penawaran'],
        //         // 'pemenang' => $value['nama_penyedia'],
        //         // 'kontrak_nilai' => $value['nilai_kontrak'],
        //         // 'realisasi_pdn' => $value['nilai_pdn_kontrak'],
        //         // 'realisasi_umk' => $value['nilai_umk_kontrak']

        //     ])->where('rup_id', $value['kd_rup']);
        // }

        // foreach($nilaiTender as $res => $value) {
        //     SpsePaket::where('rup_id', $value['kd_rup_paket'])
        //     ->update([
        //         'penawaran' => $value['nilai_penawaran'],
        //         'pemenang' => $value['nama_penyedia'],
        //         'nilai_terkoreksi' => $value['nilai_terkoreksi'], //add db
        //         'nilai_negosiasi' => $value['nilai_negosiasi'], //add db
        //         'kontrak_nilai' => $value['nilai_kontrak'],
        //         'realisasi_pdn' => $value['nilai_pdn_kontrak'],
        //         'realisasi_umk' => $value['nilai_umk_kontrak']
        //     ]);
        // }

        $this->output->progressFinish();
        Log::info(" Tender Selesai Cron execution!");
        $this->info(' Tender Selesai:Cron Command is working fine!');
    }
}
