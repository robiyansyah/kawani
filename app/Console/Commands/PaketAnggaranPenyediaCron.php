<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\SirupPaketAnggaranPenyedia;

class PaketAnggaranPenyediaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paketanggaranpenyedia:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Paket Anggaran Penyedia Command Execute Succesfull!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://isb.lkpp.go.id/isb/api/a5f70428-7edb-4cd3-ad4a-15bbb4e903aa/json/37412139/PaketAnggaranPenyedia1618/tipe/4:12/parameter/2024:D95');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        foreach($program as $res => $value) {
            SirupPaketAnggaranPenyedia::updateOrCreate([
                'koderup'  => $value['koderup'],
                'id_rup_client'  => $value['id_rup_client'],
                'kodekomponen'  => $value['kodekomponen'],
                'kodekegiatan'  => $value['kodekegiatan'],
                'pagu'  => $value['pagu'],
                'mak'  => $value['mak'],
                'sumberdana'  => $value['sumberdana'],
            ]);
        }
        Log::info("SirupPaketAnggaranPenyedia Cron execution!");
        $this->info('SirupPaketAnggaranPenyedia:Cron Command is working fine!');
    }
}
