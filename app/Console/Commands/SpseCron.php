<?php

namespace App\Console\Commands;

use App\Models\SpsePaket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use SplHeap;

class SpseCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://192.168.21.18:5000/api/v1/tender/rincian-paket-tender-selesai?tahun=2022&instansi=D95');
        $response = $request->getBody()->getContents();
        $program = json_decode($response, true);
        foreach($program as $res => $value) {
            SpsePaket::updateOrCreate([
                'pnt_nama' => $value['pnt_nama'],
                'tgl_sanggahan_terakhir' => $value['tgl_sanggahan_terakhir'],
                'lls_versi_lelang' => $value['lls_versi_lelang'],
                'pnt_id' => $value['pnt_id'],
                'idsatker' => $value['idsatker'],
                'idlelang' => $value['idlelang'],
                'ang_tahun' => $value['ang_tahun'],
                'pkt_nama' => $value['pkt_nama'],
                'stk_nama' => $value['stk_nama'],
                'mtd_pemilihan' => $value['mtd_pemilihan'],
                'kgr_id' => $value['kgr_id'],
                'pkt_pagu' => $value['pkt_pagu'],
                'pkt_hps' => $value['pkt_hps'],
                'tahapan' => $value['tahapan'],
                'penawaran' => $value['penawaran'],
                'pemenang' => $value['pemenang'],
                'lls_dibuat_tanggal' => $value['lls_dibuat_tanggal'],
                'lls_tgl_setuju' => $value['lls_tgl_setuju'],
                'kontrak_mulai' => $value['kontrak_mulai'],
                'kontrak_akhir' => $value['kontrak_akhir'],
                'kontrak_no' => $value['kontrak_no'],
                'kontrak_nilai' => $value['kontrak_nilai'],
                'rup_id' => $value['rup_id'],
                'created_at' => $value['created_at'],
                'updated_at' => $value['updated_at']
            ]);
        }
        Log::info("ObjekAkun Cron execution!");
        $this->info('ObjekAkun:Cron Command is working fine!');
    }
}
