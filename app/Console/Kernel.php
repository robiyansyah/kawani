<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('pokja:update')->hourly();
        // $schedule->command('objekakun:cron')->hourly();
        // $schedule->command('rinciobjekakun:cron')->hourly();
        // $schedule->command('program:cron')->hourly();
        // $schedule->command('kegiatan:cron')->hourly();
        $schedule->command('paketswakelola:cron')->hourly();
        $schedule->command('paketpenyedia:cron')->hourly();
        $schedule->command('paketanggaranswakelola:cron')->hourly();
        $schedule->command('paketanggaranpenyedia:cron')->hourly();
        $schedule->command('ecatalog:cron')->hourly();
        $schedule->command('tenderselesai:cron')->hourly();
        $schedule->command('tenderekontrak:cron')->hourly();
        $schedule->command('tenderpengumuman:cron')->hourly();
        $schedule->command('nontenderselesai:cron')->hourly();
        $schedule->command('nontenderekontrak:cron')->hourly();
        $schedule->command('nontenderpengumuman:cron')->hourly();
        $schedule->command('pencatatan-nontender:cron')->hourly();
        $schedule->command('pencatatan-swakelola:cron')->hourly();
        $schedule->command('pencatatan-nontender-realisasi:cron')->hourly();
        $schedule->command('tokodaring:cron')->hourly();
        
        // $schedule->command('tender-all-conversion:cron')->hourly();
        $schedule->command('toko-daring-conversion:cron')->hourly();
        $schedule->command('epurchasing-all-conversion:cron')->hourly();
        $schedule->command('nontender-all-conversion:cron')->hourly();
        $schedule->command('pencatatan-nontender-all-conversion:cron')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
