<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SirupProgram extends Model
{
    protected $table = 'sirup_programs';
    protected $fillable = [
        'id_sirup',
        'kode_programs',
        'nama',
        'pagu',
        'id_client',
        'is_deleted'
    ];
}
