<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SirupRinciObjekAkun extends Model
{
    protected $table = 'sirup_rinci_objek_akun';
    protected $fillable = [
        'id_program',
        'id_kegiatan',
        'id_rinci_objek_akun',
        'kode_rinciobjekakund',
        'uraian_rinciobjekakun',
        'pagu',
        'is_deleted',
        'id_client'
    ];
}
