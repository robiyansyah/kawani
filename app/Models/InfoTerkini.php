<?php

namespace App\Models;

use App\Models\Media as ModelsMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class InfoTerkini extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $table = 'info_terkini';
    protected $fillable = [
        'judul',
        'deskripsi',
        'image',
        'tanggal'
    ];

    protected $casts = ['tanggal' => 'date'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(300)
            ->height(300);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('collection_name')->singleFile();
    }

    public function fotoInfoTerkini()
    {
        return $this->hasMany(ModelsMedia::class, 'model_id', 'id');
    }
}
