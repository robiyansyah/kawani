<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SirupObjekAkun extends Model
{
    protected $table = 'sirup_objek_akun';
    protected $fillable = [
        'id_program',
        'id_kegiatan',
        'id_objek_akun',
        'kode_objekakund',
        'uraian_objekakun',
        'pagu',
        'is_deleted',
        'id_client'
    ];
}
