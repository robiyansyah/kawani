<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SatkerSirup extends Model
{
    protected $table = 'satker_sirup';
    protected $fillable = [
        'namasatker',
        'kodesatker'
    ];
}
