<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SirupKegiatan extends Model
{
    protected $table = 'sirup_kegiatans';
    protected $fillable = [
        'id_program',
        'id_kegiatan',
        'kode_kegiatans',
        'nama',
        'pagu',
        'is_deleted',
        'id_client'
    ];
}
