<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuktiMonev extends Model
{
    use HasFactory;

    protected $table = 'bukti_monev'; // Nama tabel yang digunakan oleh model

    protected $fillable = [
        'nama_file',
        'original_name',
        'tanggal_upload',
        'created_at',
        'created_by',
        'judul',
        'laporan_bulan',
    ];
}
