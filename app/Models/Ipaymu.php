<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ipaymu extends Model
{
    protected $table = 'monitoring';
    protected $fillable = [
        'no',
        'kode_rup',
        'nama_paket',
        'pagu',
        'metode_pemilihan',
        'penyedia',
        'sumber_dana',
        'jenis_pengadaan',
        'status_pemilihan',
        'hps',
        'pemenang_berkontrak',
        'nilai_kontrak',
        'tanggal_kontrak',
        'waktu_pelaksanan',
        'pho',
        'pp_target',
        'pp_realisasi',
        'pp_deviasi',
        'pk_target',
        'pk_realisasi',
        'pk_deviasi',
        'kendala',
        'keterangan'
    ];
}
