<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tender extends Model
{

    protected $table = 'tender';
    protected $fillable = [
        'tahun_anggaran',
        'kd_klpd',
        'nama_klpd',
        'jenis_klpd',
        'kd_satker',
        'nama_satker',
        'kd_lpse',
        'nama_lpse',
        'kd_nontender',
        'kd_rup_paket',
        'nama_paket',
        'pagu',
        'hps',
        'nilai_penawaran',
        'nilai_terkoreksi',
        'nilai_negosiasi',
        'nilai_kontrak',
        'anggaran',
        'kualifikasi_paket',
        'kategori_pengadaan',
        'metode_pengadaan',
        'tanggal_buat_paket',
        'tanggal_pengumuman_nontender',
        'tanggal_selesai_nontender',
        'kd_penyedia',
        'nama_penyedia',
        'npwp_penyedia',
        'kode_mak',
        'nilai_pdn_kontrak',
        'nilai_umk_kontrak'
    ];
}
