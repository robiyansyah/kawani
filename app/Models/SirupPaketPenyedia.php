<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SirupPaketPenyedia extends Model
{

    protected $table = 'sirup_paket_penyedia';
    protected $fillable = [
        'klpd',
        'tahunanggaran',
        'idrup',
        'namapaket',
        'jumlahpagu',
        'namasatker',
        'kodesatker',
        'metodepengadaan',
        'idmetodepengadaan',
        'jenispengadaan',
        'idjenispengadaan',
        'spesifikasi',
        'lokasi',
        'tanggalkebutuhan',
        'tanggalawalpemilihan',
        'tanggalakhirpemilihan',
        'tanggalawalpekerjaan',
        'tanggalakhirpekerjaan',
        'statuspradipa',
        'statuspenyedia',
        'statustkdn',
        'statususahakecil',
        'statusumumkan',
        'keterangan',
        'ppk',
        'username',
        'tanggalpengumuman',
        'id_swakelola',
        'statusdeletepaket',
        'statusaktifpaket',
        'id_rup_client',
        'kd_klpd',
        'nip_ppk',
        'volume',
        'lokasi_kab',
        'lokasi_prov',
        'statuspaketkonsolidasi',
        'idsatker',
        'jenis_klpd',
        'alasan_non_ukm',
        'urarian_pekerjaan',
        'tgl_awal_pemanfaatan',
        'tgl_akhir_pemanfaatan',
        'tgl_buat_paket',
        'username_ppk'
    ];

}
