<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kepatuhan extends Model
{
    protected $fillable = [
        'kode', 'dimensi', 'indikator', 'parameter', 'eviden', 'periode', 'tahun','responden'
    ];
}
