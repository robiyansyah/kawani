<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SirupPaketAnggaranPenyedia extends Model
{
    protected $table = 'sirup_paket_anggaran_penyedia';
    protected $fillable = [
        'koderup',
        'id_rup_client',
        'kodekomponen',
        'kodekegiatan',
        'pagu',
        'mak',
        'sumberdana'
    ];
}
