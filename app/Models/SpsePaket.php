<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpsePaket extends Model
{
    protected $table = 'spse_paket';
    protected $fillable = [
        'pnt_nama',
        'tgl_sanggahan_terakhir',
        'lls_versi_lelang',
        'pnt_id',
        'idsatker',
        'idlelang',
        'ang_tahun',
        'pkt_nama',
        'stk_nama',
        'mtd_pemilihan',
        'kgr_id',
        'pkt_pagu',
        'pkt_hps',
        'tahapan',
        'penawaran',
        'pemenang',
        'lls_dibuat_tanggal',
        'lls_tgl_setuju',
        'kontrak_mulai',
        'kontrak_akhir',
        'kontrak_no',
        'kontrak_nilai',
        'rup_id',
        'realisasi_pdn',
        'realisasi_umk',
        'kd_klpd',
        'nama_klpd',
        'jenis_klpd',
        'kd_satker_str',
        'kd_lpse',
        'nama_lpse',
        'kd_pkt_dce',
        'sumber_dana',
        'mak',
        'kualifikasi_paket',
        'mtd_pemilihan',
        'mtd_evaluasi',
        'mtd_kualifikasi',
        'kontrak_pembayaran',
        'tgl_penetapan_pemenang',
        'nilai_terkoreksi',
        'nilai_negosiasi',
        'created_at',
        'updated_at'
    ];
}
