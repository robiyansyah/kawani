<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SirupPaketSwakelola extends Model
{
    protected $table = 'sirup_paket_swakelola';
    protected $fillable = [
        'klpd',
        'tahunanggaran',
        'idrup',
        'namapaket',
        'jumlahpagu',
        'namasatker',
        'kodesatker',
        'lokasi',
        'tanggalawalpekerjaan',
        'tanggalakhirpekerjaan',
        'keterangan',
        'ppk',
        'nip_ppk',
        'tanggalpengumuman',
        'statusdeletepaket',
        'statusaktifpaket',
        'statusumumkan',
        'id_rup_client',
        'tipe_swakelola',
        'kode_klpd_penyelenggara',
        'nama_klpd_penyelenggara',
        'nama_satker_penyelenggara',
        'username',
        'kd_klpd',
        'volume',
        'lokasi_kab',
        'lokasi_prov',
        'idsatker'
    ];
}
