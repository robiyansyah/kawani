<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SirupPaketAnggaranSwakelola extends Model
{
    protected $table = 'sirup_paket_anggaran_swakelola';
    protected $fillable = [
        'koderup',
        'id_rup_client',
        'kodekomponen',
        'kodekegiatan',
        'pagu',
        'mak',
        'sumberdana'
    ];
}
