<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConvertPencatatanNontender extends Model
{
    use HasFactory;

    protected $table = 'convert_pencatatan_nontender';

    protected $fillable = [
        'idrup',
        'namapaket',
        'namasatker',
        'tahunanggaran',
        'jumlahpagu',
        // tambahkan atribut lainnya sesuai kebutuhan
    ];
}

