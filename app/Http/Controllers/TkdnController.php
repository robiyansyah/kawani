<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTkdnRequest;
use App\Http\Requests\UpdateTkdnRequest;
use App\Models\Tkdn;

class TkdnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTkdnRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTkdnRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tkdn  $tkdn
     * @return \Illuminate\Http\Response
     */
    public function show(Tkdn $tkdn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tkdn  $tkdn
     * @return \Illuminate\Http\Response
     */
    public function edit(Tkdn $tkdn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTkdnRequest  $request
     * @param  \App\Models\Tkdn  $tkdn
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTkdnRequest $request, Tkdn $tkdn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tkdn  $tkdn
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tkdn $tkdn)
    {
        //
    }
}
