<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class Category extends Controller
{
    protected $connection = 'dbspse';

    public function listCategory(Request $request)
    {
        $data = DB::table('categori_campaigns')
        ->selectRaw('categori_campaigns.id,
        categori_campaigns.name,
        media.id as id_media,
        media.file_name,
        categori_campaigns.id as value,
        categori_campaigns.name as text')
        ->leftJoin('media', 'media.model_id', 'categori_campaigns.id')
        ->where('media.collection_name', 'icon_categori')
        ->orderBy('categori_campaigns.created_at', 'asc')
        ->get();

        foreach ($data as $value) {
            $value->path = '/storage/'.$value->id_media.'/'.$value->file_name;
        }

        return $data;
    }

    public function daftarPaket(Request $request)
    {
        $namasatker = $request->get('namasatker');
        $nip = $request->get('nip');

        if ($nip == null) {
            $data = DB::select(DB::raw(
                "select spp.ppk, spp.klpd, spp.idrup, spp.namapaket, sp.tahapan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.statustkdn, spp.jenispengadaan, coalesce(sp.pkt_hps, 0) as pkt_hps,
                                    sp.pemenang, coalesce(sp.kontrak_nilai, 0) as kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, coalesce(m.pk_target, 0) as pk_target,
                                    coalesce(m.pk_realisasi, 0) as pk_realisasi, m.pk_deviasi, m.kendala, m.nilai_pdn, m.keterangan, coalesce(sp.kontrak_nilai, 0) as komitmen_pdn, coalesce(sp.realisasi_pdn, 0) as realisasi_pdn,
                                    coalesce(m.tkdn_persiapan_pengadaan, 0) as tkdn_persiapan_pengadaan, coalesce(m.tkdn_persiapan_pemilihan, 0) as tkdn_persiapan_pemilihan, coalesce(m.tkdn_kontrak, 0) as tkdn_kontrak, coalesce(m.tkdn_serah_terima, 0) as tkdn_serah_terima,
                                    btpp.nama_file as file_bukti_tkdn_persiapan_pengadaan, btpp.tanggal_upload as tgl_bukti_tkdn_persiapan_pengadaan,
                                    btk.nama_file as file_bukti_tkdn_kontrak, btk.tanggal_upload as tgl_bukti_tkdn_kontrak,
                                    bts.nama_file as file_bukti_tkdn_serahterima, bts.tanggal_upload as tgl_bukti_tkdn_serahterima, bkk.nama_file as file_bukti_kktkdn, bkk.tanggal_upload as tgl_file_bukti_kktkdn,
                                    'false' as edit,
                                    m.komitmen_pdn_perencanaan, m.paket_pdn, m.komitmen_pdn_pelaksanaan
                                from sirup_paket_penyedia spp
                                left join spse_paket sp on sp.rup_id = spp.idrup
                                left join monitoring m on m.kode_rup = spp.idrup
                                left join bukti_tkdn_persiapan_pengadaan btpp on btpp.idrup = spp.idrup
                                left join bukti_tkdn_kontrak btk on btk.idrup = spp.idrup
                                left join bukti_tkdn_serahterima bts on bts.idrup = spp.idrup
                                left join bukti_kertas_kerja bkk on bkk.idrup = spp.idrup
                                where spp.statusumumkan = 'Terumumkan' and spp.statustkdn = 'PDN'
                                order by spp.namasatker asc, spp.ppk asc"
                )
                // , array(
                //     'nip' => $nip
                // )
                );

                // foreach ($data as $key => $value) {
                //     $value->pagu = floatval($value->jumlahpagu);
                // }

                return $data;
        } else {
            $data = DB::select(DB::raw(
            "select spp.ppk, spp.klpd, spp.idrup, spp.namapaket, sp.tahapan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.statustkdn, spp.jenispengadaan, coalesce(sp.pkt_hps, 0) as pkt_hps,
            sp.pemenang, coalesce(sp.kontrak_nilai, 0) as kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, coalesce(m.pk_target, 0) as pk_target,
            coalesce(m.pk_realisasi, 0) as pk_realisasi, m.pk_deviasi, m.kendala, m.nilai_pdn, m.keterangan, coalesce(spp.jumlahpagu, 0) as komitmen_pdn, coalesce(sp.realisasi_pdn, 0) as realisasi_pdn,
            coalesce(m.tkdn_persiapan_pengadaan, 0) as tkdn_persiapan_pengadaan, coalesce(m.tkdn_persiapan_pemilihan, 0) as tkdn_persiapan_pemilihan, coalesce(m.tkdn_kontrak, 0) as tkdn_kontrak, coalesce(m.tkdn_serah_terima, 0) as tkdn_serah_terima,
            btpp.nama_file as file_bukti_tkdn_persiapan_pengadaan, btpp.tanggal_upload as tgl_bukti_tkdn_persiapan_pengadaan,
            btk.nama_file as file_bukti_tkdn_kontrak, btk.tanggal_upload as tgl_bukti_tkdn_kontrak, bkk.nama_file as file_bukti_kktkdn, bkk.tanggal_upload as tgl_file_bukti_kktkdn,
            bts.nama_file as file_bukti_tkdn_serahterima, bts.tanggal_upload as tgl_bukti_tkdn_serahterima,'false' as edit,
            m.komitmen_pdn_perencanaan, m.paket_pdn, m.komitmen_pdn_pelaksanaan
        from sirup_paket_penyedia spp
        left join spse_paket sp on sp.rup_id = spp.idrup
        left join monitoring m on m.kode_rup = spp.idrup
        left join bukti_tkdn_persiapan_pengadaan btpp on btpp.idrup = spp.idrup
        left join bukti_tkdn_kontrak btk on btk.idrup = spp.idrup
        left join bukti_tkdn_serahterima bts on bts.idrup = spp.idrup
        left join bukti_kertas_kerja bkk on bkk.idrup = spp.idrup
        where spp.statusumumkan = 'Terumumkan'and spp.nip_ppk = :nip and spp.statustkdn = 'PDN'
        order by spp.namasatker asc, spp.ppk asc"
            )
            , array(
                'nip' => $nip
            )
            );
            // foreach ($data as $key => $value) {
            //     $value->pagu = floatval($value->jumlahpagu);
            // }
            return $data;
        }

    }

    public function daftarPaketReview(Request $request)
    {
        $namasatker = $request->get('namasatker');
        $nip = $request->get('nip');

        if ($namasatker == null) {
            $data = DB::select(DB::raw(
                "select spp.ppk, spp.klpd, spp.idrup, spp.namapaket, sp.tahapan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.statustkdn, spp.jenispengadaan, coalesce(sp.pkt_hps, 0) as pkt_hps,
                    sp.pemenang, coalesce(sp.kontrak_nilai, 0) as kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, coalesce(m.pk_target, 0) as pk_target,
                    coalesce(m.pk_realisasi, 0) as pk_realisasi, m.pk_deviasi, m.kendala, m.nilai_pdn, m.keterangan, coalesce(spp.jumlahpagu, 0) as komitmen_pdn, coalesce(sp.realisasi_pdn, 0) as realisasi_pdn,
                    coalesce(m.tkdn_persiapan_pengadaan, 0) as tkdn_persiapan_pengadaan, coalesce(m.tkdn_persiapan_pemilihan, 0) as tkdn_persiapan_pemilihan, coalesce(m.tkdn_kontrak, 0) as tkdn_kontrak, coalesce(m.tkdn_serah_terima, 0) as tkdn_serah_terima,
                    btpp.nama_file as file_bukti_tkdn_persiapan_pengadaan, btpp.tanggal_upload as tgl_bukti_tkdn_persiapan_pengadaan,
                    btk.nama_file as file_bukti_tkdn_kontrak, btk.tanggal_upload as tgl_bukti_tkdn_kontrak, bkk.nama_file as file_bukti_kktkdn, bkk.tanggal_upload as tgl_file_bukti_kktkdn,
                    bts.nama_file as file_bukti_tkdn_serahterima, bts.tanggal_upload as tgl_bukti_tkdn_serahterima,'true' as edit,
                    m.komitmen_pdn_perencanaan, m.paket_pdn, m.komitmen_pdn_pelaksanaan
                from sirup_paket_penyedia spp
                left join spse_paket sp on sp.rup_id = spp.idrup
                left join monitoring m on m.kode_rup = spp.idrup
                left join bukti_tkdn_persiapan_pengadaan btpp on btpp.idrup = spp.idrup
                left join bukti_tkdn_kontrak btk on btk.idrup = spp.idrup
                left join bukti_tkdn_serahterima bts on bts.idrup = spp.idrup
                left join bukti_kertas_kerja bkk on bkk.idrup = spp.idrup
                where spp.statusumumkan = 'Terumumkan' and spp.statustkdn = 'PDN'
                order by spp.namasatker asc, spp.ppk asc"
                )
                // , array(
                //     'nip' => $nip
                // )
                );

                return $data;
        } else {
            $data = DB::select(DB::raw(
            "select spp.ppk, spp.klpd, spp.idrup, spp.namapaket, sp.tahapan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.statustkdn, spp.jenispengadaan, coalesce(sp.pkt_hps, 0) as pkt_hps,
                sp.pemenang, coalesce(sp.kontrak_nilai, 0) as kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, coalesce(m.pk_target, 0) as pk_target,
                coalesce(m.pk_realisasi, 0) as pk_realisasi, m.pk_deviasi, m.kendala, m.nilai_pdn, m.keterangan, coalesce(spp.jumlahpagu, 0) as komitmen_pdn, coalesce(sp.realisasi_pdn, 0) as realisasi_pdn,
                coalesce(m.tkdn_persiapan_pengadaan, 0) as tkdn_persiapan_pengadaan, coalesce(m.tkdn_persiapan_pemilihan, 0) as tkdn_persiapan_pemilihan, coalesce(m.tkdn_kontrak, 0) as tkdn_kontrak, coalesce(m.tkdn_serah_terima, 0) as tkdn_serah_terima,
                btpp.nama_file as file_bukti_tkdn_persiapan_pengadaan, btpp.tanggal_upload as tgl_bukti_tkdn_persiapan_pengadaan,
                btk.nama_file as file_bukti_tkdn_kontrak, btk.tanggal_upload as tgl_bukti_tkdn_kontrak, bkk.nama_file as file_bukti_kktkdn, bkk.tanggal_upload as tgl_file_bukti_kktkdn,
                bts.nama_file as file_bukti_tkdn_serahterima, bts.tanggal_upload as tgl_bukti_tkdn_serahterima,'true' as edit,
                m.komitmen_pdn_perencanaan, m.paket_pdn, m.komitmen_pdn_pelaksanaan
            from sirup_paket_penyedia spp
            left join spse_paket sp on sp.rup_id = spp.idrup
            left join monitoring m on m.kode_rup = spp.idrup
            left join bukti_tkdn_persiapan_pengadaan btpp on btpp.idrup = spp.idrup
            left join bukti_tkdn_kontrak btk on btk.idrup = spp.idrup
            left join bukti_tkdn_serahterima bts on bts.idrup = spp.idrup
            left join bukti_kertas_kerja bkk on bkk.idrup = spp.idrup
            where spp.statusumumkan = 'Terumumkan' and spp.namasatker = :namasatker and spp.statustkdn = 'PDN'
            order by spp.namasatker asc, spp.ppk asc"
            )
            , array(
                'namasatker' => $namasatker
            )
            );

            return $data;
        }

    }

    public function dashboardProgresPerPaket(Request $request)
    {
        if ($request->filled('namasatker')) $namaSatker = $request->input('namasatker');
        if ($request->filled('jenispaket')) $jenisPaket = $request->input('jenispaket');
        if ($request->filled('status')) $status = $request->input('status');

        if ($status = 'tender') {
            if ($jenisPaket == 'Proyek Strategis') {
                $data = DB::select(DB::raw("
                    select spp.idrup, spp.namapaket, m.status_pemilihan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.jenispengadaan, sp.pkt_hps,
                        sp.pemenang, sp.kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, m.pk_target ,
                        m.pk_realisasi, m.pk_deviasi, m.kendala, m.keterangan
                    from proyek_strategis ps
                    left join spse_paket sp on sp.rup_id = ps.idrup
                    left join monitoring m on m.kode_rup = ps.idrup
                    left join sirup_paket_penyedia spp on spp.idrup = ps.idrup
                    where spp.statusumumkan = 'Terumumkan' and spp.namasatker = :namasatker and ps.status_proyek_strategis = true
                    and spp.metodepengadaan like '%Tender%' or spp.metodepengadaan = 'Seleksi' or spp.metodepengadaan = 'Penunjukan Langsung'
                    order by spp.namasatker asc
                "
                ), array(
                    'namasatker' => $namaSatker
                )
                );
                return $data;
            } elseif ($jenisPaket == 'Terealisasi') {
                $data = DB::select(DB::raw("
                select * from (
                    select spp.idrup, spp.namapaket, m.status_pemilihan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.jenispengadaan, sp.pkt_hps,
                                    sp.pemenang, sp.kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, m.pk_target ,
                                    m.pk_realisasi, m.pk_deviasi, m.kendala, m.keterangan
                                from sirup_paket_penyedia spp
                                left join spse_paket sp on sp.rup_id = spp.idrup
                                left join monitoring m on m.kode_rup = spp.idrup
                                left join proyek_strategis ps on ps.idrup = spp.idrup
                                where spp.statusumumkan = 'Terumumkan'
                                and spp.metodepengadaan like '%Tender%' or spp.metodepengadaan = 'Seleksi' or spp.metodepengadaan = 'Penunjukan Langsung'
                                order by spp.namasatker asc
                ) as datasirup
                where  datasirup.namasatker like :namasatker and datasirup.pk_realisasi notnull
                "
                ), array(
                    'namasatker' => $namaSatker
                )
                );
                return $data;
            } elseif ($jenisPaket == 'Belum Terealisasi') {
                $data = DB::select(DB::raw("
                select * from (
                    select spp.idrup, spp.namapaket, m.status_pemilihan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.jenispengadaan, sp.pkt_hps,
                                    sp.pemenang, sp.kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, m.pk_target ,
                                    m.pk_realisasi, m.pk_deviasi, m.kendala, m.keterangan
                                from sirup_paket_penyedia spp
                                left join spse_paket sp on sp.rup_id = spp.idrup
                                left join monitoring m on m.kode_rup = spp.idrup
                                left join proyek_strategis ps on ps.idrup = spp.idrup

                                where spp.statusumumkan = 'Terumumkan'
                                and spp.metodepengadaan like '%Tender%' or spp.metodepengadaan = 'Seleksi' or spp.metodepengadaan = 'Penunjukan Langsung'
                                order by spp.namasatker asc
                ) as datasirup
                where  datasirup.namasatker like :namasatker
                "
                ), array(
                    'namasatker' => $namaSatker
                )
                );
                return $data;
            } elseif ($jenisPaket == 'Terlambat') {
                $data = DB::select(DB::raw("
                select * from (
                    select spp.idrup, spp.namapaket, m.status_pemilihan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.jenispengadaan, sp.pkt_hps,
                                    sp.pemenang, sp.kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, m.pk_target ,
                                    m.pk_realisasi, m.pk_deviasi, m.kendala, m.keterangan
                                from sirup_paket_penyedia spp
                                left join spse_paket sp on sp.rup_id = spp.idrup
                                left join monitoring m on m.kode_rup = spp.idrup
                                left join proyek_strategis ps on ps.idrup = spp.idrup

                                where spp.statusumumkan = 'Terumumkan'
                                and spp.metodepengadaan like '%Tender%' or spp.metodepengadaan = 'Seleksi' or spp.metodepengadaan = 'Penunjukan Langsung'
                                order by spp.namasatker asc
                ) as datasirup
                where  datasirup.namasatker like :namasatker and datasirup.pp_deviasi <= -5
                "
                ), array(
                    'namasatker' => $namaSatker
                )
                );
                return $data;
            }
        } elseif ($status = 'nontender') {
            if ($jenisPaket == 'Proyek Strategis') {
                $data = DB::select(DB::raw("
                    select spp.idrup, spp.namapaket, m.status_pemilihan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.jenispengadaan, sp.pkt_hps,
                        sp.pemenang, sp.kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, m.pk_target ,
                        m.pk_realisasi, m.pk_deviasi, m.kendala, m.keterangan
                    from proyek_strategis ps
                    left join spse_paket sp on sp.rup_id = ps.idrup
                    left join monitoring m on m.kode_rup = ps.idrup
                    left join sirup_paket_penyedia spp on spp.idrup = ps.idrup
                    where spp.statusumumkan = 'Terumumkan' and spp.namasatker = :namasatker and ps.status_proyek_strategis = true
                    and spp.metodepengadaan not like '%Tender%' or spp.metodepengadaan != 'Seleksi' or spp.metodepengadaan != 'Penunjukan Langsung'
                    order by spp.namasatker asc
                "
                ), array(
                    'namasatker' => $namaSatker
                )
                );
                return $data;
            } elseif ($jenisPaket == 'Terealisasi') {
                $data = DB::select(DB::raw("
                select * from (
                    select spp.idrup, spp.namapaket, m.status_pemilihan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.jenispengadaan, sp.pkt_hps,
                                    sp.pemenang, sp.kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, m.pk_target ,
                                    m.pk_realisasi, m.pk_deviasi, m.kendala, m.keterangan
                                from sirup_paket_penyedia spp
                                left join spse_paket sp on sp.rup_id = spp.idrup
                                left join monitoring m on m.kode_rup = spp.idrup
                                left join proyek_strategis ps on ps.idrup = spp.idrup

                                where spp.statusumumkan = 'Terumumkan'
                                and spp.metodepengadaan not like '%Tender%' or spp.metodepengadaan != 'Seleksi' or spp.metodepengadaan != 'Penunjukan Langsung'
                                order by spp.namasatker asc
                ) as datasirup
                where  datasirup.namasatker like :namasatker and datasirup.pk_realisasi notnull
                "
                ), array(
                    'namasatker' => $namaSatker
                )
                );
                return $data;
            } elseif ($jenisPaket == 'Belum Terealisasi') {
                $data = DB::select(DB::raw("
                select * from (
                    select spp.idrup, spp.namapaket, m.status_pemilihan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.jenispengadaan, sp.pkt_hps,
                                    sp.pemenang, sp.kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, m.pk_target ,
                                    m.pk_realisasi, m.pk_deviasi, m.kendala, m.keterangan
                                from sirup_paket_penyedia spp
                                left join spse_paket sp on sp.rup_id = spp.idrup
                                left join monitoring m on m.kode_rup = spp.idrup
                                left join proyek_strategis ps on ps.idrup = spp.idrup

                                where spp.statusumumkan = 'Terumumkan'
                                and spp.metodepengadaan not like '%Tender%' or spp.metodepengadaan != 'Seleksi' or spp.metodepengadaan != 'Penunjukan Langsung'
                                order by spp.namasatker asc
                ) as datasirup
                where  datasirup.namasatker like :namasatker
                "
                ), array(
                    'namasatker' => $namaSatker
                )
                );
                return $data;
            } elseif ($jenisPaket == 'Terlambat') {
                $data = DB::select(DB::raw("
                select * from (
                    select spp.idrup, spp.namapaket, m.status_pemilihan, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.jenispengadaan, sp.pkt_hps,
                                    sp.pemenang, sp.kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, m.pk_target ,
                                    m.pk_realisasi, m.pk_deviasi, m.kendala, m.keterangan
                                from sirup_paket_penyedia spp
                                left join spse_paket sp on sp.rup_id = spp.idrup
                                left join monitoring m on m.kode_rup = spp.idrup
                                left join proyek_strategis ps on ps.idrup = spp.idrup

                                where spp.statusumumkan = 'Terumumkan'
                                and spp.metodepengadaan like '%Tender%' or spp.metodepengadaan = 'Seleksi' or spp.metodepengadaan = 'Penunjukan Langsung'
                                order by spp.namasatker asc
                ) as datasirup
                where  datasirup.namasatker like :namasatker and datasirup.pp_deviasi <= -5
                "
                ), array(
                    'namasatker' => $namaSatker
                )
                );
                return $data;
            }
        }




    }

    public function dashboardProgresPerDinas(Request $request)
    {
        $data = DB::select(DB::raw("
            select spp.namasatker, coalesce(sum(spp.jumlahpagu), 0) as jumlahpagu, coalesce(sum(sp.pkt_hps), 0) as pkt_hps, coalesce(sum(sp.kontrak_nilai), 0) as kontrak_nilai,
            coalesce(sum(m.pk_target), 0) as pk_target, coalesce(sum(m.pk_realisasi), 0) as pk_realisasi, coalesce(sum(m.pk_deviasi), 0) as pk_deviasi, max(m.updated_at) as update_terakhir,
            count(m.pk_realisasi) as total_input_realisasi, count(case when coalesce(m.pk_realisasi,0) = 0 then 1 else 0 end) as total_blm_input_realisasi
            from sirup_paket_penyedia spp
            left join spse_paket sp on sp.rup_id = spp.idrup
            left join monitoring m on m.kode_rup = spp.idrup
            where spp.statusumumkan = 'Terumumkan'
            group by spp.namasatker
            order by spp.namasatker asc
          "
          )
        );

        return $data;
    }

    public function satker(Request $request)
    {
        $data = DB::select(DB::raw("
            select * from satker_sirup
            order by namasatker asc
          "
          )
        );

        return $data;
    }

    public function login(Request $request)
    {
        // $request->validate([
        //     'email' => 'required|string|email',
        //     'password' => 'required|string',
        // ]);
        // $res = json_decode($request->getContent());

        $username = $request->nip;
        $password = $request->password;
        // // $email = $request->input('name');
        // // $password = $request->input('password');

        // $credentials = $request->only('email', 'password');

        if (Auth::attempt(['nip' => $username, 'password' => $password])) {
            $data = DB::table('users')
                ->where('nip', $username)
                ->update([
                    'is_login' => true
                ]);
            $user = Auth::user();
            $response = [
                'name' => $user->name,
                'user_name' => $user->user_name,
                'nip' => $user->nip,
                'satker' => $user->satker,
                'role' => $user->role,
                'status' => 'success'
            ];
            return response()->json(['status' => 'success',
                                     'data' => $response], 200);
        }
        else {
            return response()->json(['status' => 'failed'], 433);
        }
    }

    public function logout(Request $request)
    {
        // $request->validate([
        //     'email' => 'required|string|email',
        //     'password' => 'required|string',
        // ]);

        $res = json_decode($request->getContent());

        $email = $request->input('name');

        $cekdatalogin = DB::table('users')
                ->selectRaw('is_login')
                ->where('email', $email)
                ->get();
        $a = json_decode($cekdatalogin);

            // TODO LIST
        // $res = $a[0]->is_login;

        // if ($res == true) {
            $data = DB::table('users')
                ->where('email', $email)
                ->update([
                    'is_login' => false
                ]);

        //     return response()->json(['status' => 'berhasil logout'], 200);

        // } elseif ($res == false) {
        //     return response()->json(['status' => 'anda sedang tidak login'], 200);
        // }
        return $data;

    }

    public function paketSPSE(Request $request)
    {
        $tahun = $request->get('tahun');

        $data=DB::connection($this->connection)
            ->select(DB::raw("
            select tenderProses.pnt_nama, tenderProses.tgl_sanggahan_terakhir, tenderProses.lls_versi_lelang, tenderProses.pnt_id, tenderProses.idSatker, tenderProses.idLelang, tenderProses.ang_tahun, tenderProses.pkt_nama, tenderProses.stk_nama, tenderProses.mtd_pemilihan, tenderProses.kgr_id, tenderProses.pkt_pagu, tenderProses.pkt_hps, tenderProses.tahapan, 0 as penawaran, '' as pemenang, date(tenderProses.lls_dibuat_tanggal) as lls_dibuat_tanggal, date(tenderProses.lls_tgl_setuju) as lls_tgl_setuju, null as kontrak_mulai, null as kontrak_akhir,
            '' as kontrak_no, 0 as kontrak_nilai, tenderProses.rup_id from (
            select panitia.pnt_nama, max(sanggahan.sgh_tanggal) as tgl_sanggahan_terakhir, ls.lls_versi_lelang, p.pnt_id, sk.stk_id as idSatker, ls.lls_id as idLelang, p.pkt_id as idPaket,anggaran.ang_tahun, p.pkt_nama, sk.stk_nama, ls.mtd_pemilihan, p.kgr_id,
            p.pkt_pagu, p.pkt_hps, Date(lls_dibuat_tanggal) as lls_dibuat_tanggal, Date(lls_tgl_setuju) as lls_tgl_setuju, ls.kirim_pengumuman_pemenang, ls.kirim_pengumuman_pemenang_pra, 'Proses' as tahapan, ps.rup_id from lelang_seleksi ls
            left join paket p on p.pkt_id = ls.pkt_id
            left join paket_satker ps on ps.pkt_id = p.pkt_id
            left join satuan_kerja sk on sk.stk_id = ps.stk_id
            left join paket_anggaran on paket_anggaran.pkt_id = p.pkt_id
            left join anggaran on anggaran.ang_id = paket_anggaran.ang_id
            left join peserta on peserta.lls_id = ls.lls_id
            left join panitia on panitia.pnt_id = p.pnt_id
            left join sanggahan on sanggahan.psr_id = peserta.psr_id
            where sk.instansi_id = 'D95' and ls.lls_status = 1 and sk.stk_nama not like '%APBN%'
            and anggaran.ang_tahun = :tahun
            group by p.pnt_id, sk.stk_id, ls.lls_id, p.pkt_id, anggaran.ang_tahun, p.pkt_nama, sk.stk_nama, ls.mtd_pemilihan,
            p.pkt_pagu, p.pkt_hps, lls_dibuat_tanggal, lls_tgl_setuju, ls.kirim_pengumuman_pemenang, ls.kirim_pengumuman_pemenang_pra, panitia.pnt_nama, ps.rup_id
            ) tenderProses
            full join(
            select panitia.pnt_nama, max(sanggahan.sgh_tanggal) as tgl_sanggahan_terakhir, ls.lls_versi_lelang, sk.stk_id as idSatker, ls.lls_id as idLelang, anggaran.ang_tahun, p.pkt_nama, sk.stk_nama, ls.mtd_pemilihan, p.kgr_id, p.pkt_pagu, p.pkt_hps, 'Tender Selesai' as tahapan, peserta.psr_harga_terkoreksi as penawaran, rekanan.rkn_nama as pemenang, date(lls_dibuat_tanggal) as lls_dibuat_tanggal, date(lls_tgl_setuju) as lls_tgl_setuju, kontrak.kontrak_mulai, kontrak.kontrak_akhir,
            kontrak.kontrak_no, kontrak.kontrak_nilai, 'Tender Selesai' as tahapan, ps.rup_id from lelang_seleksi ls
            left join paket p on p.pkt_id = ls.pkt_id
            left join paket_satker ps on ps.pkt_id = p.pkt_id
            left join satuan_kerja sk on sk.stk_id = ps.stk_id
            left join peserta on peserta.lls_id = ls.lls_id
            left join rekanan on rekanan.rkn_id = peserta.rkn_id
            left join kontrak on kontrak.lls_id = ls.lls_id
            left join paket_anggaran on paket_anggaran.pkt_id = p.pkt_id
            left join anggaran on anggaran.ang_id = paket_anggaran.ang_id
            left join panitia on panitia.pnt_id = p.pnt_id
            left join sanggahan on sanggahan.psr_id = peserta.psr_id
            where sk.instansi_id = 'D95' and ls.lls_status = 1 and peserta.is_pemenang = 1
            and anggaran.ang_tahun = 2022 and sk.stk_nama not like '%APBN%'
            group by panitia.pnt_nama, p.pnt_id, sk.stk_id, ls.lls_id, p.pkt_id, p.pkt_nama, sk.stk_nama, ls.mtd_pemilihan, anggaran.ang_tahun, peserta.psr_harga_terkoreksi, rekanan.rkn_nama, lls_dibuat_tanggal, lls_tgl_setuju, kontrak.kontrak_mulai, kontrak.kontrak_akhir, kontrak.kontrak_no, kontrak.kontrak_nilai, ps.rup_id
            ) tenderSelesai
            on tenderProses.idlelang = tenderSelesai.idlelang
            WHERE tenderProses.idlelang IS NULL
            OR tenderSelesai.idlelang IS null
            union
            select panitia.pnt_nama, max(sanggahan.sgh_tanggal) as tgl_sanggahan_terakhir, ls.lls_versi_lelang, p.pnt_id, sk.stk_id as idSatker, ls.lls_id as idLelang, anggaran.ang_tahun, p.pkt_nama, sk.stk_nama, ls.mtd_pemilihan, p.kgr_id, p.pkt_pagu, p.pkt_hps, 'Tender Selesai' as tahapan, peserta.psr_harga_terkoreksi as penawaran, rekanan.rkn_nama as pemenang, date(lls_dibuat_tanggal) as lls_dibuat_tanggal, date(lls_tgl_setuju) as lls_tgl_setuju, kontrak.kontrak_mulai, kontrak.kontrak_akhir,
            kontrak.kontrak_no, kontrak.kontrak_nilai, ps.rup_id from lelang_seleksi ls
            left join paket p on p.pkt_id = ls.pkt_id
            left join paket_satker ps on ps.pkt_id = p.pkt_id
            left join satuan_kerja sk on sk.stk_id = ps.stk_id
            left join peserta on peserta.lls_id = ls.lls_id
            left join rekanan on rekanan.rkn_id = peserta.rkn_id
            left join kontrak on kontrak.lls_id = ls.lls_id
            left join paket_anggaran on paket_anggaran.pkt_id = p.pkt_id
            left join anggaran on anggaran.ang_id = paket_anggaran.ang_id
            left join panitia on panitia.pnt_id = p.pnt_id
            left join sanggahan on sanggahan.psr_id = peserta.psr_id
            where sk.instansi_id = 'D95' and ls.lls_status = 1 and peserta.is_pemenang = 1
            and anggaran.ang_tahun = :tahun and sk.stk_nama not like '%APBN%'
            group by panitia.pnt_nama, p.pnt_id, sk.stk_id, ls.lls_id, p.pkt_id, p.pkt_nama, sk.stk_nama, ls.mtd_pemilihan, anggaran.ang_tahun, peserta.psr_harga_terkoreksi, rekanan.rkn_nama, lls_dibuat_tanggal, lls_tgl_setuju, kontrak.kontrak_mulai, kontrak.kontrak_akhir, kontrak.kontrak_no, kontrak.kontrak_nilai, ps.rup_id
                "
            ), array(
                'tahun' => $tahun
            )
            );

        return $data;
    }

    public function saveProgres(Request $request)
    {
        // $request = json_decode($r->getContent());
        $idrup = $request->idrup;
        $jenispengadaan = $request->jenispengadaan;
        $jumlahpagu = $request->jumlahpagu;
        $kendala = $request->kendala;
        $keterangan = $request->keterangan;
        $kontrak_mulai = $request->kontrak_mulai;
        $kontrak_nilai = $request->kontrak_nilai;
        $metodepengadaan = $request->metodepengadaan;
        $mtd_pemilihan = $request->mtd_pemilihan;
        $namapaket = $request->namapaket;
        $namasatker = $request->namasatker;
        $pemenang = $request->pemenang;
        $pho = $request->pho;
        $pk_deviasi = $request->pk_deviasi;
        $pk_realisasi = $request->pk_realisasi;
        $pk_target = $request->pk_target;
        $pkt_hps = $request->pkt_hps;
        $pp_deviasi = $request->pp_deviasi;
        $pp_realisasi = $request->pp_realisasi;
        $pp_target = $request->pp_target;
        $nilai_pdn = $request->nilai_pdn;
        $status_pemilihan = $request->status_pemilihan;
        $sumberdana = $request->sumberdana;
        $waktu_pelaksanan = $request->waktu_pelaksanan;
        $rencana_pdn = $request->rencana_pdn;
        $realisasi_pdn = $request->realisasi_pdn;
        $tkdn_persiapan_pengadaan = $request->tkdn_persiapan_pengadaan;
        $tkdn_persiapan_pemilihan = $request->tkdn_persiapan_pemilihan;
        $tkdn_kontrak = $request->tkdn_kontrak;
        $tkdn_serah_terima = $request->tkdn_serah_terima;
        $komitmen_pdn_perencanaan = $request->komitmen_pdn_perencanaan;
        $paket_pdn = $request->paket_pdn;
        $komitmen_pdn_pelaksanaan = $request->komitmen_pdn_pelaksanaan;
        $paketProgres = DB::table('monitoring')->where('kode_rup', $idrup)->first();
//        $user = Auth::id();

        $query = DB::transaction(function() use ($idrup,$jenispengadaan,$jumlahpagu,$kendala,$keterangan,$kontrak_mulai,$kontrak_nilai,$metodepengadaan,$mtd_pemilihan,$namapaket,$namasatker,$pemenang,$pho,$pk_deviasi,$pk_realisasi,$pk_target,$pkt_hps,$pp_deviasi, $pp_realisasi,$pp_target,$status_pemilihan,$sumberdana,$waktu_pelaksanan, $paketProgres, $nilai_pdn, $rencana_pdn,$realisasi_pdn,$tkdn_persiapan_pengadaan,$tkdn_persiapan_pemilihan,$tkdn_kontrak,$tkdn_serah_terima, $komitmen_pdn_perencanaan, $paket_pdn, $komitmen_pdn_pelaksanaan
        ) {

            if (!empty($paketProgres)) {
                $data = DB::table('monitoring')
                ->where('kode_rup', $idrup)
                ->update([
                    'kendala' => $kendala,
                    'keterangan' => $keterangan,
                    'pho' => $pho,
                    'pk_deviasi' => $pk_deviasi,
                    'pk_realisasi' => $pk_realisasi,
                    'pk_target' => $pk_target,
                    'pp_deviasi' => $pp_deviasi,
                    'pp_realisasi' => $pp_realisasi,
                    'pp_target' => $pp_target,
                    'status_pemilihan' => $status_pemilihan,
                    'nilai_pdn' => $nilai_pdn,
                    'waktu_pelaksanan' => $waktu_pelaksanan,
                    'rencana_pdn' => $rencana_pdn,
                    'realisasi_pdn' => $realisasi_pdn,
                    'tkdn_persiapan_pengadaan' => $tkdn_persiapan_pengadaan,
                    'tkdn_persiapan_pemilihan' => $tkdn_persiapan_pemilihan,
                    'tkdn_kontrak' => $tkdn_kontrak,
                    'tkdn_serah_terima' => $tkdn_serah_terima,
                    'komitmen_pdn_perencanaan' => $komitmen_pdn_perencanaan,
                    'paket_pdn' => $paket_pdn,
                    'komitmen_pdn_pelaksanaan' => $komitmen_pdn_pelaksanaan,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            } elseif (empty($paketProgres)) {
                $data = DB::table('monitoring')
                ->insert([
                    'kode_rup' => $idrup,
                    'kendala' => $kendala,
                    'keterangan' => $keterangan,
                    'pho' => $pho,
                    'pk_deviasi' => $pk_deviasi,
                    'pk_realisasi' => $pk_realisasi,
                    'pk_target' => $pk_target,
                    'pp_deviasi' => $pp_deviasi,
                    'pp_realisasi' => $pp_realisasi,
                    'pp_target' => $pp_target,
                    'status_pemilihan' => $status_pemilihan,
                    'nilai_pdn' => $nilai_pdn,
                    'waktu_pelaksanan' => $waktu_pelaksanan,
                    'rencana_pdn' => $rencana_pdn,
                    'realisasi_pdn' => $realisasi_pdn,
                    'tkdn_persiapan_pengadaan' => $tkdn_persiapan_pengadaan,
                    'tkdn_persiapan_pemilihan' => $tkdn_persiapan_pemilihan,
                    'tkdn_kontrak' => $tkdn_kontrak,
                    'tkdn_serah_terima' => $tkdn_serah_terima,
                    'komitmen_pdn_perencanaan' => $komitmen_pdn_perencanaan,
                    'paket_pdn' => $paket_pdn,
                    'komitmen_pdn_pelaksanaan' => $komitmen_pdn_pelaksanaan,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            }

        });
        return ($query) ? "error||Terjadi kesalahan, data gagal disimpan" : "success||data berhasil disimpan" ;
    }

    public function dashboardChartSummary(Request $request)
    {
        // if ($request->filled('namasatker')) $namaSatker = $request->input('namasatker');

        // $data = DB::table('sirup_paket_penyedia as spp')
        //     ->selectRaw("spp.namasatker,
        //                 count(ps.idrup) as paket_strategis,
        //                 count(m.pk_realisasi) as terealisasi,
        //                 count(case when coalesce(m.pk_realisasi,0) = 0 then 1 else 0 end) as belum_terealisasi,
        //                 sum(spp.jumlahpagu) as pagu")
        //     ->leftJoin('spse_paket as sp', 'sp.rup_id', '=', 'spp.idrup')
        //     ->leftJoin('monitoring as m', 'm.kode_rup', '=', 'spp.idrup')
        //     ->leftJoin('proyek_strategis as ps', 'ps.idrup', '=', 'spp.idrup')
        //     ->leftJoin('sirup_paket_anggaran_penyedia as spap', 'spap.koderup', '=', 'spp.idrup')
        //     ->where([
        //         ['spp.statusumumkan', 'Terumumkan'],
        //         ['spp.metodepengadaan', 'like', '%Tender%'],
        //         ['spp.metodepengadaan', 'like', '%Seleksi%'],
        //         ['spp.metodepengadaan', 'like', '%Penunjukan Langsung%']
        //     ])
        //     ->groupBy('spp.namasatker')
        //     ->orderBy('spp.namasatker', 'asc')
        //     ->get();
            // if (!empty($namaSatker)) $data = $data->where('spp.namasatker', $namaSatker);

        $data = DB::select(DB::raw(
            "select spp.klpd, spp.namasatker, coalesce(sum(spp.jumlahpagu), 0) as jumlahpagu, coalesce(sum(sp.pkt_hps), 0) as pkt_hps, coalesce(sum(sp.kontrak_nilai), 0) as kontrak_nilai,
            coalesce(sum(m.pk_target), 0) as pk_target, coalesce(sum(m.pk_realisasi), 0) as pk_realisasi, coalesce(sum(m.pk_deviasi), 0) as pk_deviasi, max(m.updated_at) as update_terakhir,
            count(ps.idrup) as total_ps, count(m.pk_realisasi) as total_input_realisasi, count(case when coalesce(m.pk_realisasi,0) = 0 then 1 else 0 end) as total_blm_input_realisasi
            from sirup_paket_penyedia spp
            left join spse_paket sp on sp.rup_id = spp.idrup
            left join monitoring m on m.kode_rup = spp.idrup
            where spp.statusumumkan = 'Terumumkan'
            group by spp.namasatker, spp.klpd
            order by spp.namasatker asc"
        //     "
        // select allPaket.namasatker, allPaket.paket_strategis, allPaket.terealisasi,
        // allPaket.belum_terealisasi,
        // allPaket.pagu,
        // dataTerlambat.terlambat
        // from (
        //     SELECT allPaket.namasatker,
        //                         count(allPaket.status_proyek_strategis) as paket_strategis,
        //                         count(allPaket.pk_realisasi) as terealisasi,
        //                         count(case when coalesce(allPaket.pk_realisasi,0) = 0 then 1 else 0 end) as belum_terealisasi,
        //                         sum(allPaket.jumlahpagu) as pagu
        //     FROM (
        //                 select *
        //           from sirup_paket_penyedia spp
        //           left join spse_paket sp on sp.rup_id = spp.idrup
        //           left join monitoring m on m.kode_rup = spp.idrup
        //           left join proyek_strategis ps on ps.idrup = spp.idrup
        //
        //           where
        //                spp.metodepengadaan like '%Tender%' or spp.metodepengadaan = 'Seleksi' or spp.metodepengadaan = 'Penunjukan Langsung'
        //           order by spp.namasatker asc
        //             ) as allPaket
        //     WHERE allPaket.statusumumkan = 'Terumumkan' and allPaket.tahunanggaran = 2022
        //     GROUP BY allPaket.namasatker
        //     ) allPaket
        //     left join (
        //         SELECT allPaket.namasatker,
        //                 count(case when coalesce(allPaket.pp_deviasi,0) = 0 then allPaket.pp_deviasi else 0 end) as terlambat
        //         FROM (
        //             select *
        //               from sirup_paket_penyedia spp
        //               left join spse_paket sp on sp.rup_id = spp.idrup
        //               left join monitoring m on m.kode_rup = spp.idrup
        //               left join proyek_strategis ps on ps.idrup = spp.idrup
        //
        //               where
        //                    spp.metodepengadaan like '%Tender%' or spp.metodepengadaan = 'Seleksi' or spp.metodepengadaan = 'Penunjukan Langsung'
        //               order by spp.namasatker asc
        //                 ) as allPaket
        //         WHERE allPaket.statusumumkan = 'Terumumkan' and allPaket.tahunanggaran = 2022 and allPaket.pp_deviasi < -5
        //         GROUP BY allPaket.namasatker
        //      ) as dataTerlambat
        //      on dataTerlambat.namasatker = allPaket.namasatker
        //   "
          )
        );

        return $data;
    }

    public function dashboardChartSummryNontender(Request $request)
    {
        $data = DB::select(DB::raw("
        select allPaket.namasatker, allPaket.paket_strategis, allPaket.terealisasi,
        allPaket.belum_terealisasi,
        allPaket.pagu,
        dataTerlambat.terlambat
        from (
            SELECT allPaket.namasatker,
                                count(allPaket.status_proyek_strategis) as paket_strategis,
                                count(allPaket.pk_realisasi) as terealisasi,
                                count(case when coalesce(allPaket.pk_realisasi,0) = 0 then 1 else 0 end) as belum_terealisasi,
                                sum(allPaket.jumlahpagu) as pagu
            FROM (
                        select *
                  from sirup_paket_penyedia spp
                  left join spse_paket sp on sp.rup_id = spp.idrup
                  left join monitoring m on m.kode_rup = spp.idrup
                  left join proyek_strategis ps on ps.idrup = spp.idrup

                  where
                       spp.metodepengadaan not like '%Tender%' or spp.metodepengadaan != 'Seleksi' or spp.metodepengadaan != 'Penunjukan Langsung'
                  order by spp.namasatker asc
                    ) as allPaket
            WHERE allPaket.statusumumkan = 'Terumumkan' and allPaket.tahunanggaran = 2022
            GROUP BY allPaket.namasatker
            ) allPaket
            left join (
                SELECT allPaket.namasatker,
                count(case when coalesce(allPaket.pp_deviasi,0) = 0 then allPaket.pp_deviasi else 0 end) as terlambat
                FROM (
                    select *
                      from sirup_paket_penyedia spp
                      left join spse_paket sp on sp.rup_id = spp.idrup
                      left join monitoring m on m.kode_rup = spp.idrup
                      left join proyek_strategis ps on ps.idrup = spp.idrup

                      where
                           spp.metodepengadaan not like '%Tender%' or spp.metodepengadaan != 'Seleksi' or spp.metodepengadaan != 'Penunjukan Langsung'
                      order by spp.namasatker asc
                        ) as allPaket
                WHERE allPaket.statusumumkan = 'Terumumkan' and allPaket.tahunanggaran = 2022 and allPaket.pp_deviasi < -5
                GROUP BY allPaket.namasatker
             ) as dataTerlambat
             on dataTerlambat.namasatker = allPaket.namasatker

          "
          )
        );

        return $data;
    }

    public function buktitkdnPersiapanPengadaan(Request $request)
    {
        $user = Auth::id();
        if ($request->filled('idrup')) $idrup = $request->input('idrup');
        $fileName = time().'.'.$request->file->getClientOriginalExtension();
        $originFileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('upload/tkdn-persiapan-pemilihan'), $fileName);
        $paketId = DB::table('bukti_tkdn_persiapan_pengadaan')->where('idrup', $idrup)->first();

        $query = DB::transaction(function() use (
          $fileName,
          $originFileName,
          $idrup,
          $paketId,
          $user) {

              if (!empty($paketId)) {
                $query = DB::table('bukti_tkdn_persiapan_pengadaan')->where('idrup', $idrup)
                ->update([
                  'nama_file' => $fileName,
                  'original_name' => $originFileName,
                  'tanggal_upload' => date('Y-m-d H:i:s'),
                  'created_at' => date('Y-m-d H:i:s'),
                  'created_by' => $user
                ]);

              }
              if (empty($paketId)) {
                $data = DB::table('bukti_tkdn_persiapan_pengadaan')->insert([
                    'nama_file' => $fileName,
                    'original_name' => $originFileName,
                    'idrup' => $idrup,
                    'tanggal_upload' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $user
                ]);
              }
          });

          return ($query) ? "error||Terjadi kesalahan, data gagal disimpan" : "success||data berhasil disimpan" ;

    }

    public function buktitkdnKontrak(Request $request)
    {
        $user = Auth::id();
        if ($request->filled('idrup')) $idrup = $request->input('idrup');
        $fileName = time().'.'.$request->file->getClientOriginalExtension();
        $originFileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('upload/tkdn-kontrak'), $fileName);
        $paketId = DB::table('bukti_tkdn_kontrak')->where('idrup', $idrup)->first();

        $query = DB::transaction(function() use (
          $fileName,
          $originFileName,
          $idrup,
          $paketId,
          $user) {

              if (!empty($paketId)) {
                $query = DB::table('bukti_tkdn_kontrak')->where('idrup', $idrup)
                ->update([
                  'nama_file' => $fileName,
                  'original_name' => $originFileName,
                  'tanggal_upload' => date('Y-m-d H:i:s'),
                  'created_at' => date('Y-m-d H:i:s'),
                  'created_by' => $user
                ]);

              }
              if (empty($paketId)) {
                $data = DB::table('bukti_tkdn_kontrak')->insert([
                    'nama_file' => $fileName,
                    'original_name' => $originFileName,
                    'idrup' => $idrup,
                    'tanggal_upload' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $user
                ]);
              }
          });

          return ($query) ? "error||Terjadi kesalahan, data gagal disimpan" : "success||data berhasil disimpan" ;

    }

    public function buktitkdnSerahTerima(Request $request)
    {
        $user = Auth::id();
        if ($request->filled('idrup')) $idrup = $request->input('idrup');
        $fileName = time().'.'.$request->file->getClientOriginalExtension();
        $originFileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('upload/tkdn-serah-terima'), $fileName);
        $paketId = DB::table('bukti_tkdn_serahterima')->where('idrup', $idrup)->first();

        $query = DB::transaction(function() use (
          $fileName,
          $originFileName,
          $idrup,
          $paketId,
          $user) {

              if (!empty($paketId)) {
                $query = DB::table('bukti_tkdn_serahterima')->where('idrup', $idrup)
                ->update([
                  'nama_file' => $fileName,
                  'original_name' => $originFileName,
                  'tanggal_upload' => date('Y-m-d H:i:s'),
                  'created_at' => date('Y-m-d H:i:s'),
                  'created_by' => $user
                ]);

              }
              if (empty($paketId)) {
                $data = DB::table('bukti_tkdn_serahterima')->insert([
                    'nama_file' => $fileName,
                    'original_name' => $originFileName,
                    'idrup' => $idrup,
                    'tanggal_upload' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $user
                ]);
              }
          });

          return ($query) ? "error||Terjadi kesalahan, data gagal disimpan" : "success||data berhasil disimpan" ;

    }

    public function buktiKKTKDN(Request $request)
    {
        $user = Auth::id();
        if ($request->filled('idrup')) $idrup = $request->input('idrup');
        $fileName = time().'.'.$request->file->getClientOriginalExtension();
        $originFileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('upload/kktkdn'), $fileName);
        $paketId = DB::table('bukti_kertas_kerja')->where('idrup', $idrup)->first();

        $query = DB::transaction(function() use (
          $fileName,
          $originFileName,
          $idrup,
          $paketId,
          $user) {

              if (!empty($paketId)) {
                $query = DB::table('bukti_kertas_kerja')->where('idrup', $idrup)
                ->update([
                  'nama_file' => $fileName,
                  'original_name' => $originFileName,
                  'tanggal_upload' => date('Y-m-d H:i:s'),
                  'created_at' => date('Y-m-d H:i:s'),
                  'created_by' => $user
                ]);

              }
              if (empty($paketId)) {
                $data = DB::table('bukti_kertas_kerja')->insert([
                    'nama_file' => $fileName,
                    'original_name' => $originFileName,
                    'idrup' => $idrup,
                    'tanggal_upload' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $user
                ]);
              }
          });

          return ($query) ? "error||Terjadi kesalahan, data gagal disimpan" : "success||data berhasil disimpan" ;

    }

    public function rankingRealisasi(Request $request)
    {
        $data = DB::select(DB::raw(
            "select spp.klpd, spp.namasatker, coalesce(sum(spp.jumlahpagu), 0) as jumlahpagu, coalesce(sum(sp.pkt_hps), 0) as pkt_hps, coalesce(sum(sp.kontrak_nilai), 0) as kontrak_nilai,
            coalesce(sum(m.pk_target), 0) as pk_target, coalesce(sum(m.pk_realisasi), 0) as pk_realisasi, coalesce(sum(m.pk_deviasi), 0) as pk_deviasi, max(m.updated_at) as update_terakhir,
            count(m.pk_realisasi) as total_input_realisasi, count(case when coalesce(m.pk_realisasi,0) = 0 then 1 else 0 end) as total_blm_input_realisasi
            ,coalesce(sum(sp.kontrak_nilai),0) as komitmen_pdn, coalesce(sum(sp.realisasi_pdn), 0) as realisasi_pdn, coalesce(sum(pkt_hps), 0) as rencana_tkdn, coalesce(sum(m.tkdn_serah_terima), 0) as realisasi_tkdn
            from sirup_paket_penyedia spp
            left join spse_paket sp on sp.rup_id = spp.idrup
            left join monitoring m on m.kode_rup = spp.idrup
            where spp.statusumumkan = 'Terumumkan'
            group by spp.namasatker, spp.klpd
            order by spp.namasatker asc"
          )
        );

        return $data;
    }

    public function rekapAllDinas(Request $request)
    {
        $data = DB::select(DB::raw(
            "select sum(paket.jumlahpagu) as nilai_rencana, count(paket.jumlahpagu) as paket_rencana, sum(paket.kontrak_nilai) as nilai_realisasi, count(paket.pemenang) as paket_realisasi, sum(paket.realisasi_pdn) as nilai_realisasi_pdn, sum(paket.rencana_pdn) as nilai_rencana_pdn
            from (
              select distinct spp.ppk, spp.klpd, spp.idrup, spp.namapaket, spp.namasatker, spp.jumlahpagu, spp.metodepengadaan, sp.mtd_pemilihan, spp.statustkdn, spp.jenispengadaan, coalesce(sp.pkt_hps, 0) as pkt_hps,
                sp.pemenang, coalesce(sp.kontrak_nilai, 0) as kontrak_nilai, sp.kontrak_mulai, m.waktu_pelaksanan, m.pho, m.pp_target, m.pp_realisasi, m.pp_deviasi, coalesce(m.pk_target, 0) as pk_target,
                coalesce(m.pk_realisasi, 0) as pk_realisasi, m.pk_deviasi, m.kendala, m.nilai_pdn, m.keterangan,
                m.rencana_pdn, sp.realisasi_pdn, m.tkdn_persiapan_pengadaan, m.tkdn_persiapan_pemilihan, m.tkdn_kontrak, m.tkdn_serah_terima,
                btpp.nama_file as file_bukti_tkdn_persiapan_pengadaan, btpp.tanggal_upload as tgl_bukti_tkdn_persiapan_pengadaan,
                btk.nama_file as file_bukti_tkdn_kontrak, btk.tanggal_upload as tgl_bukti_tkdn_kontrak,
                bts.nama_file as file_bukti_tkdn_serahterima, bts.tanggal_upload as tgl_bukti_tkdn_serahterima,'false' as edit
            from sirup_paket_penyedia spp
            left join spse_paket sp on sp.rup_id = spp.idrup
            left join monitoring m on m.kode_rup = spp.idrup

            left join bukti_tkdn_persiapan_pengadaan btpp on btpp.idrup = spp.idrup
            left join bukti_tkdn_kontrak btk on btk.idrup = spp.idrup
            left join bukti_tkdn_serahterima bts on bts.idrup = spp.idrup
            where spp.statusumumkan = 'Terumumkan' and spp.tahunanggaran = '2023'
            order by spp.namasatker asc, spp.ppk asc
            ) as paket"
          )
        );

        return $data;
    }

    public function perbandinganPaketPDN(Request $request)
    {
        $data = DB::select(DB::raw(
            "select *
            from (
                select spp.klpd , count(spp.jumlahpagu) as paket_pdn, sum(spp.jumlahpagu) as nilai_pdn
                from sirup_paket_penyedia spp
                where spp.statusumumkan = 'Terumumkan' and spp.statustkdn = 'PDN' and spp.tahunanggaran = '2023'
                group by spp.klpd )
            as pdn
            left join (
                select spp.klpd , count(spp.jumlahpagu) as paket_non_pdn, sum(spp.jumlahpagu) as nilai_non_pdn
                from sirup_paket_penyedia spp
                where spp.statusumumkan = 'Terumumkan' and spp.statustkdn = 'Non-PDN' and spp.tahunanggaran = '2023'
                group by spp.klpd
            ) as nonpdn
            on pdn.klpd = nonpdn.klpd"
          )
        );

        return $data;
    }

    public function top5pdn(Request $request)
    {
        $data = DB::select(DB::raw(
            "select spp.namasatker , count(spp.jumlahpagu) as paket_pdn, sum(spp.jumlahpagu) as nilai_pdn
            from sirup_paket_penyedia spp
            where spp.statusumumkan = 'Terumumkan' and spp.statustkdn = 'PDN' and spp.tahunanggaran = '2023'
            group by spp.namasatker
            order by nilai_pdn desc limit 5"
          )
        );

        return $data;
    }

    public function paketPDNbyMetodePengadaan(Request $request)
    {
        $data = DB::select(DB::raw(
            "select spp.metodepengadaan , count(spp.jumlahpagu) as paket_pdn, sum(spp.jumlahpagu) as nilai_pdn
            from sirup_paket_penyedia spp
            where spp.statusumumkan = 'Terumumkan' and spp.statustkdn = 'PDN' and spp.tahunanggaran = '2023'
            group by spp.metodepengadaan
            order by spp.metodepengadaan asc"
          )
        );

        return $data;
    }
    public function progresPerkategori(Request $request)
    {
        $data = DB::select(DB::raw(
            " select paket.metodepengadaan, sum(paket.jumlahpagu) as pagu, sum(coalesce(paket.realisasi_pdn, 0)) as realisasi_pdn
            from (
	            select spp.klpd, spp.metodepengadaan, spp.namasatker, coalesce(sum(spp.jumlahpagu), 0) as jumlahpagu, coalesce(sum(sp.pkt_hps), 0) as pkt_hps, coalesce(sum(sp.kontrak_nilai), 0) as kontrak_nilai,
            coalesce(sum(m.pk_target), 0) as pk_target, coalesce(sum(m.pk_realisasi), 0) as pk_realisasi, coalesce(sum(m.pk_deviasi), 0) as pk_deviasi, max(m.updated_at) as update_terakhir
            , count(m.pk_realisasi) as total_input_realisasi, count(case when coalesce(m.pk_realisasi,0) = 0 then 1 else 0 end) as total_blm_input_realisasi
            ,coalesce(sum(sp.kontrak_nilai),0) as komitmen_pdn, coalesce(sum(sp.realisasi_pdn), 0) as realisasi_pdn, coalesce(sum(pkt_hps), 0) as rencana_tkdn, coalesce(sum(m.tkdn_serah_terima), 0) as realisasi_tkdn
	            from sirup_paket_penyedia spp
	            left join spse_paket sp on sp.rup_id = spp.idrup
	            left join monitoring m on m.kode_rup = spp.idrup

	            left join bukti_tkdn_persiapan_pengadaan btpp on btpp.idrup = spp.idrup
	            left join bukti_tkdn_kontrak btk on btk.idrup = spp.idrup
	            left join bukti_tkdn_serahterima bts on bts.idrup = spp.idrup
	            where spp.statusumumkan = 'Terumumkan' and spp.tahunanggaran = '2023'
	            group by spp.klpd, spp.namasatker, spp.metodepengadaan
            order by spp.klpd, spp.namasatker asc
            ) as paket
            group by paket.metodepengadaan"
          )
        );

        return $data;
    }

    public function totalPaketPengadaan(Request $request)
    {
        $data = DB::select(DB::raw(
            "select sum(spp.jumlahpagu) as pagu, count(spp.jumlahpagu) as paket
            from sirup_paket_penyedia spp
            where spp.statusumumkan = 'Terumumkan' and spp.tahunanggaran = '2023'
            "
          )
        );

        return $data;
    }

    public function realisasiPengadaan(Request $request)
    {
        $data = DB::select(DB::raw(
            "select sum(sp.pkt_pagu) as nilai, count(sp.rup_id) as total_paket from spse_paket sp
            where sp.statusumumkan is null or sp.statusumumkan = 'Terumumkan' and sp.ang_tahun = '2023'
            "
          )
        );

        return $data;
    }

    public function realisasiPDN(Request $request)
    {
        $data = DB::select(DB::raw(
            "select * from (
                select 'jabar' as klpd, sum(sp.pkt_pagu) as rencana_pdn, count(sp.rup_id) as total_paket_rencana_pdn from spse_paket sp
                where sp.statusumumkan is null or sp.statusumumkan = 'Terumumkan' and sp.ang_tahun = '2023'
                ) as a
                left join (
                select 'jabar' as klpd, sum(sp.pkt_pagu) as realisasi_pdn, count(sp.rup_id) as total_paket_realisasi_pdn from spse_paket sp
                where sp.statusumumkan is null or sp.statusumumkan = 'Terumumkan' and statustkdn = 'PDN' or statustkdn is null and sp.ang_tahun = '2023'
                ) as b
                on b.klpd = a.klpd

            "
          )
        );

        return $data;
    }

    public function realisasiTKDN(Request $request)
    {
        $data = DB::select(DB::raw(
            "select sum(a.rencana_tkdn) as rencana_tkdn, sum(a.realisasi_tkdn) as realisasi_tkdn from (
                select spp.klpd, spp.namasatker, coalesce(sum(spp.jumlahpagu), 0) as jumlahpagu, coalesce(sum(sp.pkt_hps), 0) as pkt_hps, coalesce(sum(sp.kontrak_nilai), 0) as kontrak_nilai,
                coalesce(sum(m.pk_target), 0) as pk_target, coalesce(sum(m.pk_realisasi), 0) as pk_realisasi, coalesce(sum(m.pk_deviasi), 0) as pk_deviasi, max(m.updated_at) as update_terakhir,
                count(m.pk_realisasi) as total_input_realisasi, count(case when coalesce(m.pk_realisasi,0) = 0 then 1 else 0 end) as total_blm_input_realisasi
                ,coalesce(sum(sp.kontrak_nilai),0) as komitmen_pdn, coalesce(sum(sp.realisasi_pdn), 0) as realisasi_pdn, coalesce(sum(pkt_hps), 0) as rencana_tkdn, coalesce(sum(m.tkdn_serah_terima), 0) as realisasi_tkdn
                from sirup_paket_penyedia spp
                left join spse_paket sp on sp.rup_id = spp.idrup
                left join monitoring m on m.kode_rup = spp.idrup
                where spp.statusumumkan = 'Terumumkan'
                group by spp.namasatker, spp.klpd
                order by spp.namasatker asc
                ) as a
            "
          )
        );

        return $data;
    }

    public function validasiTokoDaring(Request $request)
    {
        $data = DB::select(DB::raw(
            "select td.kd_klpd, td.nama_klpd , td.kd_satker ,
                case when td.nama_satker is null then ss.namasatker end nama_satker, td.order_id , td.order_desc , td.valuasi , td.kategori , td.metode_bayar , td.tanggal_transaksi , td.marketplace , td.nama_merchant ,
                td. jenis_transaksi , td.kota_kab , td.provinsi , td.nama_pemesan , td. status_verif , td.sumber_data , td.status_konfirmasi_ppmse , rt.id_rup
            from toko_daring td
                left join rup_tokodaring rt on rt.order_id = td.order_id
                left join satker_sirup ss on ss.kodesatker = td.kd_satker
            where td.kd_klpd = 'D95' and td.status_konfirmasi_ppmse = 'selesai' and td.status_verif  = 'verified'")
        );

        return $data;
    }

    public function summaryAll(Request $request)
    {
        $data = DB::select(DB::raw(
            "
            select
                sum(total_perencanaan) as total_perencanaan,
                sum(total_perencanaan_penyedia) as total_perencanaan_penyedia,
                sum(total_perencanaan_pdn) as total_perencanaan_pdn,
                sum(perencanaan_pdn) as perencanaan_pdn,
                sum(total_epurchasing_perencanaan) as total_epurchasing_perencanaan,
                sum(perencanaan_epurchasing) as perencanaan_epurchasing,
                sum(total_perencanaan_umk) as total_perencanaan_umk,
                sum(total_pelaksanaan) as total_pelaksanaan,
                sum(total_pelaksanaan_penyedia) as total_pelaksanaan_penyedia,
                sum(total_pelaksanaan_pdn) as total_pelaksanaan_pdn,
                sum(pelaksanaan_pdn) as pelaksanaan_pdn,
                sum(capaian_komitmen_pdn) as capaian_komitmen_pdn,
                sum(total_epurchasing_pelaksanaan) as total_epurchasing_pelaksanaan,
                sum(pelaksanaan_epurchasing) as pelaksanaan_epurchasing,
                sum(capaian_komitmen_epurchasing) as capaian_komitmen_epurchasing,
                sum(total_pelaksanaan_ekatalog) as total_pelaksanaan_ekatalog,
                sum(total_pelaksanaan_umk) as total_pelaksanaan_umk,
                sum(pdn_pelaksanaan_umk) as pdn_pelaksanaan_umk,
                sum(total_pembayaran) as total_pembayaran,
                sum(total_pembayaran_pdn) as total_pembayaran_pdn,
                sum(total_pembayaran_impor) as total_pembayaran_impor
            from bigbox
            where nama_satker notnull
            ")
        );

        return $data;
    }

}
