<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ConvertPencatatanNontender;

class ConvertPencatatanNontenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ConvertPencatatanNontender::all();
        return view('monitoring', ['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            // Tambahkan validasi sesuai dengan kebutuhan Anda
        ]);

        ConvertPencatatanNontender::create($request->all());

        return redirect()->route('monitoring.index')
            ->with('success', 'Data berhasil ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ConvertPencatatanNontender::find($id);
        return view('detail', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            // Tambahkan validasi sesuai dengan kebutuhan Anda
        ]);

        $data = ConvertPencatatanNontender::find($id);
        $data->update($request->all());

        return redirect()->route('monitoring.index')
            ->with('success', 'Data berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ConvertPencatatanNontender::find($id);
        $data->delete();

        return redirect()->route('monitoring.index')
            ->with('success', 'Data berhasil dihapus.');
    }
}
