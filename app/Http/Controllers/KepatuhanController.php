<?php

namespace App\Http\Controllers;

use App\Models\Kepatuhan;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class KepatuhanController extends Controller
{
    //
    public function index()
    {
        $kepatuhans = Kepatuhan::latest()->get();
        return response([
            'success' => true,
            'message' => 'List Semua kepatuhans',
            'data' => $kepatuhans
        ], 200);
    }

    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'kode'     => 'required',
            'dimensi'   => 'required',
            'indikator'   => 'required',
            'parameter'   => 'required',
            'eviden'   => 'required',
            'periode'   => 'required',
            'responden'   => 'required',
        ],
            [
                'kode.required' => 'Masukkan Kode!',
                'dimensi.required' => 'Masukkan Dimensi !',
                'indikator.required' => 'Masukkan Indikator  !',
                'parameter.required' => 'Masukkan Parameter  !',
                'eviden.required' => 'Masukkan Link Eviden  !',
                'periode.required' => 'Masukkan Periode  !',
                'responden.required' => 'Masukkan Responden !',
            ]
        );

        if($validator->fails()) {

            return response()->json([
                'success' => false,
                'message' => 'Silahkan Isi Bidang Yang Kosong',
                'data'    => $validator->errors()
            ],400);

        } else {

            $kepatuhan = kepatuhan::create([
                'kode'     => $request->input('kode'),
                'dimensi'   => $request->input('dimensi'),
                'indikator'   => $request->input('indikator'),
                'parameter'   => $request->input('parameter'),
                'eviden'   => $request->input('eviden'),
                'periode'   => $request->input('periode'),
                'responden'   => $request->input('responden')
                
            ]);


            if ($kepatuhan) {
                return response()->json([
                    'success' => true,
                    'message' => 'Kuesioner Berhasil Disimpan!',
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Kuesioner Gagal Disimpan!',
                ], 400);
            }
        }
    }


    public function show($id)
    {
        $kepatuhan = kepatuhan::whereId($id)->first();

        if ($kepatuhan) {
            return response()->json([
                'success' => true,
                'message' => 'Detail kepatuhan!',
                'data'    => $kepatuhan
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'kepatuhan Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }

    public function update(Request $request)
    {
        //validate data
        $id = $request->id;
        $skor = $request->skor;
        $validator = Validator::make($request->all(), [
            'skor'     => 'required',
        ],
            [
                'skor.required' => 'Masukkan Skor kepatuhan !',
            ]
        );

        if($validator->fails()) {

            return response()->json([
                'success' => false,
                'message' => 'Silahkan Isi Bidang Yang Kosong',
                'data'    => $validator->errors()
            ],400);

        } else {

            $kepatuhan = Kepatuhan::whereId($request->input('id'))->update([
                'id'     => $request->input('id'),
                'skor'     => $request->input('skor'),
            ]);


            if ($kepatuhan) {
                return response()->json([
                    'success' => true,
                    'message' => 'kepatuhan Berhasil Diupdate!',
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'kepatuhan Gagal Diupdate!',
                ], 500);
            }

        }

    }

    public function destroy($id)
    {
        $kepatuhan = Kepatuhan::findOrFail($id);
        $kepatuhan->delete();

        if ($kepatuhan) {
            return response()->json([
                'success' => true,
                'message' => 'kepatuhan Berhasil Dihapus!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'kepatuhan Gagal Dihapus!',
            ], 500);
        }

    }
}
