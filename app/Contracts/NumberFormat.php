<?php

namespace App\Contracts;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat as BaseNumberFormat;

class NumberFormat extends BaseNumberFormat
{
    const FORMAT_CUSTOM_NUMBER = '#,##0';

    const FORMAT_DATE_DDMMYYYY_STRIP = 'dd-mm-yyyy';
}
