<?php

namespace App\Exports;

use App\Contracts\NumberFormat;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use DB;

class DetailExport implements FromCollection, ShouldAutoSize, WithEvents, WithHeadings
{
    use Exportable;

    public function collection()
    {
        $data = DB::select(DB::raw(
            "select a.nama_satker, b.nama_unit, b.total_perencanaan_penyedia as pagu, b.total_pelaksanaan as realisasi, b.total_pelaksanaan_penyedia as pengadaan, 
            b.total_pelaksanaan_pdn as realisasi_pdn, b.total_epurchasing_pelaksanaan as realisasi_epurchasing,
            b.total_pelaksanaan_umk as realisasi_umk
            from bigbox a, bigbox_detail b
            where a.kodesatker = b.kodesatker
            and a.tahun_anggaran = '2024'
            and a.nama_satker != 'PROVINSI JAWA BARAT'"
        )
      );

      return collect($data);
    }

    public function headings(): array
    {
        return [
            'Perangkat Daerah',
            'Nama Unit',
            'Pagu',
            'Realisasi',
            'Realisasi Pengadaan',
            'Realisasi PDN',
            'Realisasi e-Purchasing',
            'Realisasi UMK',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $style = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        ],
                    ],
                ];

                $sheet = $event->getSheet()->getDelegate();
                $highest_row = $sheet->getHighestRow();
                $highest_col = $sheet->getHighestColumn();

                $sheet->getStyle('A1:'.$highest_col.$highest_row)
                    ->applyFromArray($style);

                $sheet->getStyle('A1:'.$highest_col.'1')->getFont()->setBold(true);
                $sheet->getStyle('A1:'.$highest_col.'1')->getAlignment()
                    ->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A1:'.$highest_col.'1')->getAlignment()
                    ->setVertical(Alignment::VERTICAL_CENTER);

                $string_format_columns = ['A','B'];
                $date_format_columns = [];
                $percentage_format_columns = [];
                $number_format_columns = ['C', 'D', 'E', 'G','H'];

                for ($i = 2; $i <= $highest_row; $i++) {
                    foreach ($string_format_columns as $column) {
                        $sheet->setCellValueExplicit($column.$i, $sheet->getCell($column.$i)->getValue(), DataType::TYPE_STRING);
                    }

                    foreach ($date_format_columns as $column) {
                        $value = $sheet->getCell($column.$i)->getValue();
                        $sheet->getStyle($column.$i)
                            ->getNumberFormat()
                            ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY_STRIP);

                        if ($value) {
                            $sheet->setCellValueExplicit($column.$i, $value, DataType::TYPE_ISO_DATE);
                        }
                    }

                    foreach ($percentage_format_columns as $column) {
                        $sheet->getStyle($column.$i)
                            ->getNumberFormat()
                            ->setFormatCode(NumberFormat::FORMAT_PERCENTAGE_00);
                    }

                    foreach ($number_format_columns as $column) {
                        $sheet->getStyle($column.$i)
                            ->getNumberFormat()
                            ->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);
                    }
                }

                $sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->freezePane('A2');
                $sheet->setAutoFilter('A1:'.$highest_col.'1');
            },
        ];
    }
}