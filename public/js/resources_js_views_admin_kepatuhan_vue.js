"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_admin_kepatuhan_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/admin/kepatuhan.vue?vue&type=script&lang=js":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/admin/kepatuhan.vue?vue&type=script&lang=js ***!
  \****************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'kepatuhan'
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/admin/kepatuhan.vue?vue&type=template&id=2881ca9a":
/*!***************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/admin/kepatuhan.vue?vue&type=template&id=2881ca9a ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* binding */ render),
/* harmony export */   staticRenderFns: () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c("div", {
    staticClass: "container"
  }, [_vm._m(0), _vm._v(" "), _c("button", {
    staticClass: "btn btn-success mb-3",
    on: {
      click: function click($event) {
        return _vm.handleDownload(_vm.items);
      }
    }
  }, [_vm._v("Report")]), _vm._v(" "), _vm._m(1), _vm._v(" "), _c("div", {
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-md-6"
  }, [_c("pagination", {
    staticClass: "my-0",
    attrs: {
      "total-rows": _vm.totalRows,
      "per-page": _vm.perPage,
      align: "fill",
      size: "sm",
      pills: ""
    },
    model: {
      value: _vm.currentPage,
      callback: function callback($$v) {
        _vm.currentPage = $$v;
      },
      expression: "currentPage"
    }
  })], 1)])]);
};
var staticRenderFns = [function () {
  var _vm = this,
    _c = _vm._self._c;
  return _c("div", {
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-md-6"
  }, [_c("div", {
    staticClass: "form-group"
  }, [_c("label", {
    attrs: {
      "for": "per-page-select"
    }
  }, [_vm._v("Pilih Dimensi")]), _vm._v(" "), _c("select", {
    staticClass: "form-control"
  }, [_c("option", [_vm._v("Dimensi 1: Desain dan Implementasi Kebijakan")]), _vm._v(" "), _c("option", [_vm._v("Dimensi 2: Kelembagaan P3DN ")]), _vm._v(" "), _c("option", [_vm._v("Dimensi 3: Perencanaan Kebutuhan ")]), _vm._v(" "), _c("option", [_vm._v("Dimensi 4: Perencanaan Pengadaan ")]), _vm._v(" "), _c("option", [_vm._v("Dimensi 5: Pelaksanaan Pengadaan")])])]), _vm._v(" "), _c("div", {
    staticClass: "form-group"
  }, [_c("label", {
    attrs: {
      "for": "per-page-select"
    }
  }, [_vm._v("Pilih Indikator")]), _vm._v(" "), _c("select", {
    staticClass: "form-control"
  }, [_c("option", [_vm._v("1. Kebijakan yang mendorong belanjan PDN")]), _vm._v(" "), _c("option", [_vm._v("2. Belanja PDN sebagai IKU Unit Kerja Eselon-I/II/ setara")]), _vm._v(" "), _c("option", [_vm._v("3. Keberadaan kebijakan yang menghambat penggunaan PDN ")]), _vm._v(" "), _c("option", [_vm._v("4. Roadmap strategi peningkatan penggunaan produk dalam negeri dan produk Usaha Mikro, Usaha Kecil, dan Koperasi ")]), _vm._v(" "), _c("option", [_vm._v("5. Program pengurangan impor paling lambat pada tahun 2023 sampai dengan 5 (lima persen) bagi KLD yang masih melakukan pemenuhan belanja melalui impor ")]), _vm._v(" "), _c("option", [_vm._v("6. Kebijakan yang mendorong supply PDN ke KLPBUBL.")]), _vm._v(" "), _c("option", [_vm._v("7. Kebijakan penyelenggaraan katalog elektronik lokal/sectoral ")])])])]), _vm._v(" "), _c("div", {
    staticClass: "col-xl-3 col-md-3 mb-1"
  }, [_c("div", {
    staticClass: "card border-left-warning shadow h-100 py-2"
  }, [_c("div", {
    staticClass: "card-body"
  }, [_c("div", {
    staticClass: "row no-gutters align-items-center"
  }, [_c("div", {
    staticClass: "col mr-2"
  }, [_c("div", {
    staticClass: "h6 font-weight-bold text-warning text-uppercase mb-3"
  }, [_vm._v("\n                                  Nilai Self Assesment\n                              ")]), _vm._v(" "), _c("div", {
    staticClass: "h1 mb-0 font-weight-bold text-gray-800"
  }, [_vm._v("\n                                  97%\n                              ")])]), _vm._v(" "), _c("div", {
    staticClass: "col-mt-2"
  }, [_c("i", {
    staticClass: "fas fa-check fa-2x text-yellow-300 mt-2",
    staticStyle: {
      color: "orange"
    }
  })])])])])]), _vm._v(" "), _c("div", {
    staticClass: "col-xl-3 col-md-3 mb-1"
  }, [_c("div", {
    staticClass: "card border-left-success shadow h-100 py-2"
  }, [_c("div", {
    staticClass: "card-body"
  }, [_c("div", {
    staticClass: "row no-gutters align-items-center"
  }, [_c("div", {
    staticClass: "col mr-2"
  }, [_c("div", {
    staticClass: "h6 font-weight-bold text-success text-uppercase mb-3"
  }, [_vm._v("\n                                  Nilai Verifikasi BPKP\n                              ")]), _vm._v(" "), _c("div", {
    staticClass: "h1 mb-0 font-weight-bold text-gray-800"
  }, [_vm._v("\n                                  75%\n                              ")])]), _vm._v(" "), _c("div", {
    staticClass: "col-auto"
  }, [_c("i", {
    staticClass: "fas fa-check-double fa-2x text-green-300",
    staticStyle: {
      color: "green"
    }
  })])])])])])]);
}, function () {
  var _vm = this,
    _c = _vm._self._c;
  return _c("div", {
    staticClass: "card"
  }, [_c("table", {
    staticClass: "table table-bordered"
  }, [_c("thead", {
    staticClass: "thead-dark"
  }, [_c("tr", [_c("th", {
    staticStyle: {
      width: "80%"
    }
  }, [_vm._v("Parameter")]), _vm._v(" "), _c("th", {
    staticStyle: {
      width: "10%"
    }
  }, [_vm._v("Eviden")]), _vm._v(" "), _c("th", {
    staticStyle: {
      width: "10%"
    }
  }, [_vm._v("Skor")]), _vm._v(" "), _c("th", {
    staticStyle: {
      width: "10%"
    }
  }, [_vm._v("Aksi")])])]), _vm._v(" "), _c("tbody", [_c("tr", [_c("th", {
    staticClass: "th-sm"
  }, [_vm._v("1. Terdapat kebijakan yang mendorong belanjan PDN")]), _vm._v(" "), _c("th", [_c("a", {
    staticClass: "btn btn-primary",
    attrs: {
      href: "https://1drv.ms/f/s!AmdI1Ugu6QaCqF6NhPa4CqIV5fhb?e=3cuJo7",
      target: "_blank",
      role: "button"
    }
  }, [_vm._v("Bukti")])]), _vm._v(" "), _c("th", [_c("input", {
    attrs: {
      type: "number",
      name: "",
      id: "",
      min: "0",
      max: "100"
    }
  })]), _vm._v(" "), _c("th", [_c("input", {
    attrs: {
      type: "submit"
    }
  })])]), _vm._v(" "), _c("tr", [_c("th", [_vm._v("2. Kebijakan telah disertai dengan mekanisme pemantauan, evaluasi, serta sistem reward and punishment")]), _vm._v(" "), _c("th", [_c("a", {
    staticClass: "btn btn-primary",
    attrs: {
      href: "https://1drv.ms/f/s!AmdI1Ugu6QaCqFi1bA427b0MRlXJ?e=8wgeu1",
      target: "_blank",
      role: "button"
    }
  }, [_vm._v("Bukti")])]), _vm._v(" "), _c("th", [_c("input", {
    attrs: {
      type: "number",
      name: "",
      id: "",
      min: "0",
      max: "100"
    }
  })]), _vm._v(" "), _c("th", [_c("input", {
    attrs: {
      type: "submit"
    }
  })])]), _vm._v(" "), _c("tr", [_c("th", [_vm._v("3. Mekanisme pemantauan dan evaluasi telah dilaksanakan ditunjukkan dengan adanya laporan pemantauan dan evaluasi kinerja kebijakan ")]), _vm._v(" "), _c("th", [_c("a", {
    staticClass: "btn btn-primary",
    attrs: {
      href: "https://1drv.ms/f/s!AmdI1Ugu6QaCqDviPSWPf1VbI0Gd?e=oMW6Or",
      target: "_blank",
      role: "button"
    }
  }, [_vm._v("Bukti")])]), _vm._v(" "), _c("th", [_c("input", {
    attrs: {
      type: "number",
      name: "",
      id: "",
      min: "0",
      max: "100"
    }
  })]), _vm._v(" "), _c("th", [_c("input", {
    attrs: {
      type: "submit"
    }
  })])]), _vm._v(" "), _c("tr", [_c("th", [_vm._v("4. Sistem reward atas kepatuhan dan punishment atas ketidakpatuhan PA/KPA/Jabatan Sepadan telah dilaksanakan ditunjukkan dengan adanya kegiatan pemberian reward dan punishment")]), _vm._v(" "), _c("th", [_c("a", {
    staticClass: "btn btn-primary",
    attrs: {
      href: "https://1drv.ms/f/s!AmdI1Ugu6QaCqFyFlH_fjakINt_4?e=swL7i4",
      target: "_blank",
      role: "button"
    }
  }, [_vm._v("Bukti")])]), _vm._v(" "), _c("th", [_c("input", {
    attrs: {
      type: "number",
      name: "",
      id: "",
      min: "0",
      max: "100"
    }
  })]), _vm._v(" "), _c("th", [_c("input", {
    attrs: {
      type: "submit"
    }
  })])]), _vm._v(" "), _c("tr", [_c("th", [_vm._v("5. Efektivitas kebijakan yang mendorong belanja PDN dan UMKK telah dievaluasi secara formal ditunjukkan dengan adanya laporan hasil evaluasi ")]), _vm._v(" "), _c("th", [_c("a", {
    staticClass: "btn btn-primary",
    attrs: {
      href: "https://1drv.ms/f/s!AmdI1Ugu6QaCqDqxOdUqHD5EBx-R?e=13ixN6",
      target: "_blank",
      role: "button"
    }
  }, [_vm._v("Bukti")])]), _vm._v(" "), _c("th", [_c("input", {
    attrs: {
      type: "number",
      name: "",
      id: "",
      min: "0",
      max: "100"
    }
  })]), _vm._v(" "), _c("th", [_c("input", {
    attrs: {
      type: "submit"
    }
  })])])])])]);
}];
render._withStripped = true;


/***/ }),

/***/ "./resources/js/views/admin/kepatuhan.vue":
/*!************************************************!*\
  !*** ./resources/js/views/admin/kepatuhan.vue ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _kepatuhan_vue_vue_type_template_id_2881ca9a__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./kepatuhan.vue?vue&type=template&id=2881ca9a */ "./resources/js/views/admin/kepatuhan.vue?vue&type=template&id=2881ca9a");
/* harmony import */ var _kepatuhan_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./kepatuhan.vue?vue&type=script&lang=js */ "./resources/js/views/admin/kepatuhan.vue?vue&type=script&lang=js");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _kepatuhan_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  _kepatuhan_vue_vue_type_template_id_2881ca9a__WEBPACK_IMPORTED_MODULE_0__.render,
  _kepatuhan_vue_vue_type_template_id_2881ca9a__WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/admin/kepatuhan.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/admin/kepatuhan.vue?vue&type=script&lang=js":
/*!************************************************************************!*\
  !*** ./resources/js/views/admin/kepatuhan.vue?vue&type=script&lang=js ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_kepatuhan_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./kepatuhan.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/admin/kepatuhan.vue?vue&type=script&lang=js");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_kepatuhan_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/admin/kepatuhan.vue?vue&type=template&id=2881ca9a":
/*!******************************************************************************!*\
  !*** ./resources/js/views/admin/kepatuhan.vue?vue&type=template&id=2881ca9a ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_kepatuhan_vue_vue_type_template_id_2881ca9a__WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   staticRenderFns: () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_kepatuhan_vue_vue_type_template_id_2881ca9a__WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_kepatuhan_vue_vue_type_template_id_2881ca9a__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./kepatuhan.vue?vue&type=template&id=2881ca9a */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/admin/kepatuhan.vue?vue&type=template&id=2881ca9a");


/***/ })

}]);